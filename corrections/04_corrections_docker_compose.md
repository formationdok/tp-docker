# TP4 - Application multiconteneur - Corrigé

## 1) Mise en place d'une architecture

  * On souhaite créer une architecture comprenant un serveur _Wordpress_, adossée à une base de données _MariaDB_.
    * On souhaite utiliser les images suivantes :
      - `wordpress:6.2`
      - `mariadb:11.0`
  * Ecrire le fichier `docker-compose.yml` afin de mettre en place cette architecture. On utilisera la version `3.2` de `docker_compose.yml`. On évitera au maximum de regarder le `docker_compose.yml` de la documentation de l'image `Wordpress`...
    * Quelques indications pour _Wordpress_ :
      - On mettra la politique de redémarrage à `always`.
      - L'image `Wordpress` écoute sur le port `80`. On fera un mapping de vers le port `8085` du serveur Docker.
      - La connexion à la base de données est renseignée avec quelques variables d'environnement :
        - `WORDPRESS_DB_HOST`
        - `WORDPRESS_DB_USER`
        - `WORDPRESS_DB_PASSWORD`
        - `WORDPRESS_DB_NAME`
      - On utilisera un volume pour stocker les fichiers du site web. On utilisera un volume pour le répertoire `/var/www/html` du serveur.
    * Quelques indications pour _MariaDB_ :
      - On mettra la politique de redémarrage à `always`.
      - Utiliser les bonnes variables d'environnement pour créer l'utilisateur utilisé par _Wordpress_, ainsi qu'un mot de passe `root` aléatoire.
      - On utilisera un volume pour stocker les fichiers du site web. On utilisera un volume pour le répertoire `/var/lib/mysql` du serveur.
```
version: '3.2'

services:

  wordpress:
    image: wordpress:6.2
    restart: always
    ports:
      - 8085:80
    environment:
      WORDPRESS_DB_HOST: db
      WORDPRESS_DB_USER: formation
      WORDPRESS_DB_PASSWORD: formation
      WORDPRESS_DB_NAME: formation
    volumes:
      - wordpress:/var/www/html

  db:
    image: mariadb:11.0
    restart: always
    environment:
      MARIADB_DATABASE: formation
      MARIADB_USER: formation
      MARIADB_PASSWORD: formation
      MARIADB_RANDOM_ROOT_PASSWORD: '1'
    volumes:
      - db:/var/lib/mysql

volumes:
  wordpress:
  db:
```
  * Tester le lancement de ce fichier et le bon fonctionnement de _Wordpress_.

```
florent@LAPTOP-S2TDFR7B:~/tp-docker/corrections/dc$ ./docker-compose up -d
[+] Running 4/4
 ✔ Volume "dc_wordpress"     Created                                                                                                              0.0s 
 ✔ Volume "dc_db"            Created                                                                                                              0.0s 
 ✔ Container dc-wordpress-1  Started                                                                                                              0.7s 
 ✔ Container dc-db-1         Started                                                                                                              0.7s 
 ```
  * Faire ce qu'il faut pour récupérer le mot de passe `root` _MariaDB_ généré.
On saisit la commande `docker logs dc-db-1` pour afficher les logs du serveur _MariaDB_. Dans les logs, on retrouve une ligne indiquant le mot de passe `root` généré.

```
2023-07-30 09:17:47+00:00 [Note] [Entrypoint]: Temporary server started.
2023-07-30 09:17:49+00:00 [Note] [Entrypoint]: GENERATED ROOT PASSWORD: amprJj9;}JmZ?oPX}1Xy$&gWM:+8Z969
2023-07-30 09:17:49+00:00 [Note] [Entrypoint]: Creating database formation
```
  * Arrêter tous les services sans supprimer les volumes
```
florent@LAPTOP-S2TDFR7B:~/tp-docker/corrections/dc$ ./docker-compose down
[+] Running 3/3
 ✔ Container dc-db-1         Removed                                                                                                                                                    10.3s 
 ✔ Container dc-wordpress-1  Removed                                                                                                                                                     1.4s 
 ✔ Network dc_default        Removed                                                                                                                                                     0.3s                                                                                                                                                    0.2s
```

## 2) Amélioration de l'architecture

  * On souhaite améliorer l'architecture avec les modifications suivantes (tester le fichier après chaque amélioration ):
    * Au lieu d'utiliser un réseau créé pour l'occasion automatiquement, on souhaite utiliser un réseau `bridge` appelé `wpnet`. 

```
version: '3.2'

services:

  wordpress:
    image: wordpress:6.2
    restart: always
    ports:
      - 8085:80
    environment:
      WORDPRESS_DB_HOST: db
      WORDPRESS_DB_USER: formation
      WORDPRESS_DB_PASSWORD: formation
      WORDPRESS_DB_NAME: formation
    volumes:
      - wordpress:/var/www/html
    networks:
      - wpnet

  db:
    image: mariadb:11.0
    restart: always
    environment:
      MARIADB_DATABASE: formation
      MARIADB_USER: formation
      MARIADB_PASSWORD: formation
      MARIADB_RANDOM_ROOT_PASSWORD: '1'
    volumes:
      - db:/var/lib/mysql
    networks:
      - wpnet

networks:
  wpnet:
    driver: bridge

volumes:
  wordpress:
  db:
```

  * Changer les _hostnames_ des serveurs _Wordpress_ et _MariaDB_ comme suit et modifier le fichier `docker-compose.yml` en conséquence.
      - _Wordpress_ : `pistache`
      - _MariaDB_: `noisette`

```
version: '3.2'

services:

  wordpress:
    image: wordpress:6.2
    restart: always
    hostname: pistache
    ports:
      - 8085:80
    environment:
      WORDPRESS_DB_HOST: noisette
      WORDPRESS_DB_USER: formation
      WORDPRESS_DB_PASSWORD: formation
      WORDPRESS_DB_NAME: formation
    volumes:
      - wordpress:/var/www/html
    networks:
      - wpnet

  db:
    image: mariadb:11.0
    restart: always
    hostname: noisette
    environment:
      MARIADB_DATABASE: formation
      MARIADB_USER: formation
      MARIADB_PASSWORD: formation
      MARIADB_RANDOM_ROOT_PASSWORD: '1'
    volumes:
      - db:/var/lib/mysql
    networks:
      - wpnet

networks:
  wpnet:
    driver: bridge

volumes:
  wordpress:
  db:
```

  * Modifier le nom des conteneurs créés :
    - _Wordpress_ : `formationwp`
    - _MariaDB_: `formationdb`

```
version: '3.2'

services:

  wordpress:
    image: wordpress:6.2
    restart: always
    hostname: pistache
    container_name: formationwp
    ports:
      - 8085:80
    environment:
      WORDPRESS_DB_HOST: noisette
      WORDPRESS_DB_USER: formation
      WORDPRESS_DB_PASSWORD: formation
      WORDPRESS_DB_NAME: formation
    volumes:
      - wordpress:/var/www/html
    networks:
      - wpnet

  db:
    image: mariadb:11.0
    restart: always
    hostname: noisette
    container_name: formationdb
    environment:
      MARIADB_DATABASE: formation
      MARIADB_USER: formation
      MARIADB_PASSWORD: formation
      MARIADB_RANDOM_ROOT_PASSWORD: '1'
    volumes:
      - db:/var/lib/mysql
    networks:
      - wpnet

networks:
  wpnet:
    driver: bridge

volumes:
  wordpress:
  db:
```
  * Coupler un serveur _PHPMyAdmin_ à la base de données _MariaDB_ via l'utilisation de l'image `phpmyadmin:5.2` et ces précisions :
    - _Hostname_ du serveur : `cajou`
    - Politique de redémarrage à `always`.
    - Nom du conteneur : `formationphpma`
    - L'image `phpmyadmin` écoute sur le port `80`. On fera un mapping de vers le port `8086` du serveur Docker.
    - Trouver ce qu'il faut pour relier _PHPMyAdmin_ à _MariaDB_.

```
version: '3.2'

services:

  wordpress:
    image: wordpress:6.2
    restart: always
    hostname: pistache
    container_name: formationwp
    ports:
      - 8085:80
    environment:
      WORDPRESS_DB_HOST: noisette
      WORDPRESS_DB_USER: formation
      WORDPRESS_DB_PASSWORD: formation
      WORDPRESS_DB_NAME: formation
    volumes:
      - wordpress:/var/www/html
    networks:
      - wpnet

  db:
    image: mariadb:11.0
    restart: always
    hostname: noisette
    container_name: formationdb
    environment:
      MARIADB_DATABASE: formation
      MARIADB_USER: formation
      MARIADB_PASSWORD: formation
      MARIADB_RANDOM_ROOT_PASSWORD: '1'
    volumes:
      - db:/var/lib/mysql
    networks:
      - wpnet

  phpmyadmin:
    image: phpmyadmin:5.2
    restart: always
    hostname: cajou
    container_name: formationphpma
    environment:
      PMA_HOST: noisette
    ports:
      - 8086:80
    networks:
      - wpnet

networks:
  wpnet:
    driver: bridge

volumes:
  wordpress:
  db:
```
  * Tester la connexion à _PHPMyAdmin_

Si on se rend sur `http://localhost:8086`, on accède à l'interface de connexion de PHPMyAdmin. En se connectant avec le compte `formation`:`formation`, on peut se connecter à la base et voir les tables générées par _Wordpress_.

## 3) Utilisation d'images personnelles

Dans le TP3, exercices 2 et 3, on a créé un `Dockerfile` d'un serveur _Node.js_.

Créer le fichier `compose.yaml` permettant de faire fonctionner tout cela en y ajoutant un conteneur généré par ce `Dockerfile`. Quelques précisions :
  - _Hostname_ du serveur Node.js : `macadamia`
  - _Hostname_ du serveur MongoDB : `pistache`
  - Politique de redémarrage à `always`
  - Nom du conteneur Node.js : `formationnode`
  - Nom du conteneur MongoDB : `formationmongodb`
  - Node.js écoute sur le port `9500`. On fera un mapping de vers le port `8088` du serveur Docker  
  - On utilisera un volumme appelé `mongovol` pour persister les données MongoDB
  - On utilisera un réseau appelé `formnet`


Mettre en place l'architecture via un fichier `compose.yaml` et vérifier le bon fonctionnement de tous les conteneurs.

Pour rappel, voici le fichier `Dockerfile`

```
FROM node:16
LABEL maintainer=florent@example.com
ENV MONGO_URI=mongodb://mongo:27017
ENV PORT=6060
WORKDIR /usr/src/app
COPY . .
RUN npm ci
HEALTHCHECK --interval=15s --start-period=30s --timeout=2s CMD node healthcheck.js
ENTRYPOINT ["npm", "start"]
```

Voici le fichier `compose.yaml`

```
version: '3.2'

services:
  node:
    build: Express_Mongo
    restart: always
    hostname: macadamia
    container_name: formationnode
    depends_on:
      - db
    environment:
      PORT: 9500
      MONGO_URI: mongodb://pistache:27017
    ports:
      - 8088:9500
    networks:
      - formnet
  
  db:
    image: mongo:latest
    restart: always
    hostname: pistache
    container_name: formationmongodb
    volumes:
      - mongovol:/data/db
    networks:
      - formnet

networks:
  formnet:
    driver: bridge

volumes:
  mongovol:
```