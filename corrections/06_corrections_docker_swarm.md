# TP6 - Docker Swarm - Corrigé

## Mise en oeuvre

Pour exécuter ce TP, nous aurons besoin de plusieurs machines. Pour cela, on utilisera [_Play With Docker_](https://github.com/play-with-docker/play-with-docker).

Deux possibilités :
- Utiliser [_Play With Docker_ fourni par Docker](https://labs.play-with-docker.com/)
- Utiliser _Play With Docker_ fourni par le formateur (solution privilégiée lors des formations)

## Mise en palce d'un swarm

* Initialiser un swarm sur la première machine

La machine a pour IP `192.168.0.8`. On initialise le swarm.

```
[node1] (local) root@192.168.0.8 ~
$ docker swarm init --advertise-addr 192.168.0.8
Swarm initialized: current node (waq4vr4r72h0dfldbo10yn2ic) is now a manager.

To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-3cer5unmmkgyjizu77649ycp8e9vr0km6lsbzpptwupcaih8rn-2oxegsf5ycom4i2oyf634ln02 192.168.0.8:2377

To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.
```

* Vérifier la liste des noeuds présents

```
[node1] (local) root@192.168.0.8 ~
$ docker node ls
ID                            HOSTNAME   STATUS    AVAILABILITY   MANAGER STATUS   ENGINE VERSION
waq4vr4r72h0dfldbo10yn2ic *   node1      Ready     Active         Leader           24.0.2
```

* Vérifier via un `docker info` que la machine fait vien partie d'un swarm

```
[node1] (local) root@192.168.0.8 ~
$ docker info
Client:
 [...]

Server:
 [...]
 Plugins:
  Volume: local
  Network: bridge host ipvlan macvlan null overlay
  Log: awslogs fluentd gcplogs gelf journald json-file local logentries splunk syslog
 Swarm: active
  NodeID: waq4vr4r72h0dfldbo10yn2ic
  Is Manager: true
  ClusterID: vg9cbeqz5009rbnlpsesjjneg
  Managers: 1
  Nodes: 1
  Default Address Pool: 10.0.0.0/8  
  SubnetSize: 24
  Data Path Port: 4789
  Orchestration:
   Task History Retention Limit: 5
  Raft:
   Snapshot Interval: 10000
   Number of Old Snapshots to Retain: 0
   Heartbeat Tick: 1
   Election Tick: 10
  Dispatcher:
   Heartbeat Period: 5 seconds
  CA Configuration:
   Expiry Duration: 3 months
   Force Rotate: 0
  Autolock Managers: false
  Root Rotation In Progress: false
  Node Address: 192.168.0.8
  Manager Addresses:
   192.168.0.8:2377
 Runtimes: io.containerd.runc.v2 runc
 [...]

WARNING: API is accessible on http://0.0.0.0:2375 without encryption.
         Access to the remote API is equivalent to root access on the host. Refer
         to the 'Docker daemon attack surface' section in the documentation for
         more information: https://docs.docker.com/go/attack-surface/
WARNING: No swap limit support
WARNING: bridge-nf-call-iptables is disabled
WARNING: bridge-nf-call-ip6tables is disabled
```

* Créer 3 machines supplémentaires, récupérer le jeton pour en faire des _workers_ et les initialiser en tant que _worker_

On créé les 3 machines via le bouton **+ ADD NEW INSTANCE**.

On récupère le jeton.

```
[node1] (local) root@192.168.0.8 ~
$ docker swarm join-token worker
To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-3cer5unmmkgyjizu77649ycp8e9vr0km6lsbzpptwupcaih8rn-2oxegsf5ycom4i2oyf634ln02 192.168.0.8:2377
```

Sur chacun des _workers_, on rejoint le swarm.

```
[node4] (local) root@192.168.0.5 ~
$ docker swarm join --token SWMTKN-1-3cer5unmmkgyjizu77649ycp8e9vr0km6lsbzpptwupcaih8rn-2oxegsf5ycom4i2oyf634ln02 192.168.0.8:2377
This node joined a swarm as a worker.
```

* Vérifier la liste des noeuds présents

```
$ docker node ls
ID                            HOSTNAME   STATUS    AVAILABILITY   MANAGER STATUS   ENGINE VERSION
waq4vr4r72h0dfldbo10yn2ic *   node1      Ready     Active         Leader           24.0.2
vhv9s0y5xpqtob0e15r1s35hn     node2      Ready     Active                          24.0.2
fjjxqsggpjvmcucwwn7wusbj8     node3      Ready     Active                          24.0.2
j8fwn2pvb9u3y03usrc2fzh0x     node4      Ready     Active                          24.0.2
```

* Sur le swarm, créer un service _web_, basé sur l'image `nginx` avec 2 replicas. Le service devra exposer le port 8080 vers le port 80 des _workers_

```
$ docker service create --replicas 2 -p 8080:80 --name web nginx
gpf7xe0uc4ydx68cmwsl2zizg
overall progress: 2 out of 2 tasks 
1/2: running   [==================================================>] 
2/2: running   [==================================================>] 
verify: Service converged 
```

* Sur le _manager_, vérifier que le service est bien opérationnel et afficher ses informations

```
$ docker service ls
ID             NAME      MODE         REPLICAS   IMAGE          PORTS
gpf7xe0uc4yd   web       replicated   2/2        nginx:latest   *:8080->80/tcp
```

```
$ docker service ps web
ID             NAME      IMAGE          NODE      DESIRED STATE   CURRENT STATE                ERROR     PORTS
36tx4ju7ykp6   web.1     nginx:latest   node2     Running         Running about a minute ago             
j9vt3ubs6hk3   web.2     nginx:latest   node1     Running         Running about a minute ago   
```

* Essayer de se connecter à l'application via le port 8080 du _manager_ ou d'un _worker_

On utilise pour cela, sur une machine, le bouton **OPEN PORT** de _Play With Docker_ et on ouvre le port 8080.

## Simulation de panne

* Sur le _manager_, vérifier les noeuds sur lesquels est opérationnel `nginx`

```
$ docker service ps web
ID             NAME      IMAGE          NODE      DESIRED STATE   CURRENT STATE           ERROR     PORTS
g17ptw8axicq   web.1     nginx:latest   node1     Running         Running 5 minutes ago             
pb22itvr66mn   web.2     nginx:latest   node2     Running         Running 5 minutes ago 
```

* Couper un _worker_ sur lequel `nginx` est opérationnel

Ici, on va couper le noeud `node2`.

On sélectionne le `node2` sur _Play With Docker_ et on clique sur le bouton **DELETE**

* Vérifier que le swarm a bien remis le nombre de replicas à 2

Au bout de quelques secondes, via un `docker service ps web` on constate que le service a été déployé sur un autre noeud.

```
$ docker service ps web
ID             NAME        IMAGE          NODE      DESIRED STATE   CURRENT STATE           ERROR     PORTS
g17ptw8axicq   web.1       nginx:latest   node1     Running         Running 6 minutes ago             
69p4m12nimb5   web.2       nginx:latest   node3     Ready           Ready 1 second ago                
pb22itvr66mn    \_ web.2   nginx:latest   node2     Shutdown        Running 6 minutes ago
```

## Redimensionnement du swarm et arrêt

* Modifier le swarm pour qu'il y ait maintenant 3 replicas

```
$ docker service scale web=3
web scaled to 3
overall progress: 3 out of 3 tasks 
1/3: running   [==================================================>] 
2/3: running   [==================================================>] 
3/3: running   [==================================================>] 
verify: Service converged 
```

* Vérifier la présence de 3 replicas

```
$ docker service ps web
ID             NAME        IMAGE          NODE      DESIRED STATE   CURRENT STATE                ERROR     PORTS
laisuwvmwqn7   web.1       nginx:latest   node3     Running         Running about a minute ago             
tylkzbpv50er    \_ web.1   nginx:latest   node2     Shutdown        Running 2 minutes ago                  
k2gb16akllut   web.2       nginx:latest   node1     Running         Running 2 minutes ago                  
lf56a0dnjj73   web.3       nginx:latest   node4     Running         Running 51 seconds ago
```

* Arrêter le service `nginx` et vérifier que les conteneurs ont bien été arrêtés sur les _workers_

```
$ docker service rm web
web
```

Sur un _worker_, on note que le conteneur `nginx` a bien été supprimé.

```
$ docker ps
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
```

## Création d'un docker-compose et mise à jour

* Effacer toutes les machines créés précédemment sur _Play With Docker_

Sur chaque machine, dans _Play With Docker_, on clique sur **DELETE**

* Créer une machine sur _Play With Docker_ puis y créer un dossier `app` avec à l'intérieur :
  * Un fichier `docker-compose.yml` (vide pour le moment)
  * Un dossier `myweb` avec à l'intérieur
    * Un fichier `Dockerfile`
    * Un fichier `index.html`

  Voici le contenu du fichier `index.html`

  ```
  <h1>Version 1.0</h1>
  ```

  Voici le contenu du fichier `Dockerfile`

  ```
  FROM nginx:1.25.2
  COPY index.html /usr/share/nginx/html/
  ```

  Pour créer les fichiers et l'arborescence :

  ```
    [node1] (local) root@192.168.0.7 /
    $ mkdir app
    [node1] (local) root@192.168.0.7 /
    $ cd app
    [node1] (local) root@192.168.0.7 /app
    $ mkdir myweb
    [node1] (local) root@192.168.0.7 /app
    $ cd myweb/
    [node1] (local) root@192.168.0.7 /app/myweb
    $ vi Dockerfile
    [node1] (local) root@192.168.0.7 /app/myweb
    $ vi index.html
  ```

* Faire un build de l'image et la pousser sur le Docker Hub (éventuellement la tester avant le push)

Après avoir créé le repository sur Docker Hub

```
  $ docker build -t <id_docker>/web:1.0 .
  [+] Building 0.2s (7/7) FINISHED                                                      
  => [internal] load build definition from Dockerfile                             0.0s
  => => transferring dockerfile: 95B                                              0.0s
  => [internal] load .dockerignore                                                0.0s
  => => transferring context: 2B                                                  0.0s
  => [internal] load metadata for docker.io/library/nginx:1.25.2                  0.1s
  => [internal] load build context                                                0.0s
  => => transferring context: 31B                                                 0.0s
  => [1/2] FROM docker.io/library/nginx:1.25.2@sha256:104c7c5c54f2685f0f46f3be60  0.0s
  => CACHED [2/2] COPY index.html /usr/share/nginx/html/                          0.0s
  => exporting to image                                                           0.0s
  => => exporting layers                                                          0.0s
  => => writing image sha256:a37afbb51c39e3ca4a5fd3e3bf070e21fbc3a5d65afeb36b96b  0.0s
  => => naming to docker.io/hokebab99/myweb:1.0                                   0.0s

  $ docker login
  Login with your Docker ID to push and pull images from Docker Hub. If you don't have a Docker ID, head over to https://hub.docker.com to create one.
  Username: <id_docker>
  Password: 
  WARNING! Your password will be stored unencrypted in /root/.docker/config.json.
  Configure a credential helper to remove this warning. See
  https://docs.docker.com/engine/reference/commandline/login/#credentials-store

  Login Succeeded

  $ docker push <id_docker>/web:1.0
  The push refers to repository [docker.io/<id_docker>/web]
  a6981066aa1e: Pushed 
  563c64030925: Pushed 
  6fb960878295: Pushed 
  e161c3f476b5: Pushed 
  8a7e12012e6f: Pushed 
  d0a62f56ef41: Pushed 
  4713cb24eeff: Pushed 
  511780f88f80: Pushed 
  1.0: digest: sha256:ea4b675b6e1361a5f7fa1644a192e1884f0139dfd18cd866f29ac35bd493f4f0 size: 1985
 ```

* Ecrire le fichier `docker-compose.yml` afin qu'il utilise l'image qu'on a créé, avec un nombre de replicas à 2 (ne pas oublier d'exposer le port 8080 vers le port 80 de nos conteneurs)

 ```
    [node1] (local) root@192.168.0.7 /app/myweb
    $ cd ..
    [node1] (local) root@192.168.0.7 /app
    $ vi docker-compose.yml
  ```
  
  Voici le contenu du fichier `docker-compose.yml`

  ```
  version: "3.7"
  services:
    web:
        image: <id_docker>/web:1.0
        ports:
        - 8080:80
        deploy:
        mode: replicated
        replicas: 2
  ```

* Créer 3 machines supplémentaires sur _Play With Docker_ et initialiser un swarm avec 1 _manager_ et 3 _workers_

Voir la création du swarm avec les workers dans la première partie du TP

* Via la commande `docker stack deploy`, déployer le service décrit dans le fichier `docker-compose.yml`

```
$ docker stack deploy --compose-file docker-compose.yml myweb
Creating network myweb_default
Creating service myweb_web
```

* Vérifier que le site est accessible

On vérifie la présence du service et son déploiement

```
$ docker service ps myweb_web
ID             NAME          IMAGE                NODE      DESIRED STATE   CURRENT STATE            ERROR     PORTS
n26y51zcxscb   myweb_web.1   <id_docker>/web:1.0   node2     Running         Running 40 seconds ago             
3720zrk6ydxs   myweb_web.2   <id_docker>/web:1.0   node1     Running         Running 42 seconds ago
```

* Modifier le fichier `index.html` et changer la version en 2.0, recréer l'image et la pousser sur le repository Docker Hub

On édite le fichier `index.html` et on modifie le contenu

```
<h1>Version 2.0</h1>
```

On reconstruit l'image et on la pousse sur Docker Hub

```
[node1] (local) root@192.168.0.6 ~/app/myweb
$ docker build -t <id_docker>/web:2.0 .
[+] Building 0.7s (8/8) FINISHED                                                                                                                                               
 => [internal] load build definition from Dockerfile                                                                                                                      0.0s
 => => transferring dockerfile: 94B                                                                                                                                       0.0s
 => [internal] load .dockerignore                                                                                                                                         0.0s
 => => transferring context: 2B                                                                                                                                           0.0s
 => [internal] load metadata for docker.io/library/nginx:1.25.2                                                                                                           0.4s
 => [auth] library/nginx:pull token for registry-1.docker.io                                                                                                              0.0s
 => [internal] load build context                                                                                                                                         0.0s
 => => transferring context: 59B                                                                                                                                          0.0s
 => [1/2] FROM docker.io/library/nginx:1.25.2@sha256:104c7c5c54f2685f0f46f3be607ce60da7085da3eaa5ad22d3d9f01594295e9c                                                     0.1s
 => => resolve docker.io/library/nginx:1.25.2@sha256:104c7c5c54f2685f0f46f3be607ce60da7085da3eaa5ad22d3d9f01594295e9c                                                     0.0s
 => => sha256:eea7b3dcba7ee47c0d16a60cc85d2b977d166be3960541991f3e6294d795ed24 8.15kB / 8.15kB                                                                            0.0s
 => => sha256:104c7c5c54f2685f0f46f3be607ce60da7085da3eaa5ad22d3d9f01594295e9c 1.86kB / 1.86kB                                                                            0.0s
 => => sha256:48a84a0728cab8ac558f48796f901f6d31d287101bc8b317683678125e0d2d35 1.78kB / 1.78kB                                                                            0.0s
 => [2/2] COPY index.html /usr/share/nginx/html/                                                                                                                          0.1s
 => exporting to image                                                                                                                                                    0.0s
 => => exporting layers                                                                                                                                                   0.0s
 => => writing image sha256:f09e6a08d85af70e5277c05821658da15ff1e7172d9e131da163a6754675fc75                                                                              0.0s
 => => naming to docker.io/<id_docker>/web:2.0                                                                                                                             0.0s
[node1] (local) root@192.168.0.6 ~/app/myweb
$ docker push <id_docker>/web:2.0
The push refers to repository [docker.io/<id_docker>/web]
1752b48e35c4: Pushed 
563c64030925: Layer already exists 
6fb960878295: Layer already exists 
e161c3f476b5: Layer already exists 
8a7e12012e6f: Layer already exists 
d0a62f56ef41: Layer already exists 
4713cb24eeff: Layer already exists 
511780f88f80: Layer already exists 
2.0: digest: sha256:a1802bdadedeb706c26c2d9f5e0bb4a54262a5ff6bf06d18a5308490e795a463 size: 1985
```

* Mettre à jour le service swarm avec la version 2.0 fraichement créée

```
$ docker service update --image <id_docker>/web:2.0 myweb_web
myweb_web
overall progress: 2 out of 2 tasks 
1/2: running   [==================================================>] 
2/2: running   [==================================================>] 
verify: Service converged 
```

On note le déploiement progessif sur les noeuds.

En se connectant sur le port 8080 d'un noeud, on voit que la version 2.0 a été déployée.

