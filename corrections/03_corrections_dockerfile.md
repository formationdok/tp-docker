# TP3 - Dockerfile - Corrigé

## 1) Un premier Dockerfile

Soit le _Dockerfile_ suivant :

```
FROM ubuntu:trusty
LABEL maintainer=florent@example.com
RUN apt-get update
ENTRYPOINT ["/bin/ping", "-c", "3"]
CMD ["localhost"]
```

* Créer l'image associée à ce _Dockerfile_ en n'oubliant pas de la tagger (par exemple `formation:1.0`)

Création d'un répertoire pour cet exercice et création d'un fichier _Dockerfile_

```
florent@LAPTOP-S2TDFR7B:~$ mkdir TP3
florent@LAPTOP-S2TDFR7B:~$ cd TP3/
florent@LAPTOP-S2TDFR7B:~/TP3$ vi Dockerfile
```

Création de l'image

```
florent@LAPTOP-S2TDFR7B:~/TP3$ docker build -t formation:1.0 .
Sending build context to Docker daemon  2.048kB
Step 1/5 : FROM ubuntu:trusty
trusty: Pulling from library/ubuntu
2e6e20c8e2e6: Pull complete
0551a797c01d: Pull complete
512123a864da: Pull complete
Digest: sha256:64483f3496c1373bfd55348e88694d1c4d0c9b660dee6bfef5e12f43b9933b30
Status: Downloaded newer image for ubuntu:trusty
 ---> 13b66b487594
Step 2/5 : LABEL maintainer=florent@example.com
 ---> Running in 5f47a7a56c1b
Removing intermediate container 5f47a7a56c1b
 ---> 55895539741d
Step 3/5 : RUN apt-get update
 ---> Running in 7ea2bdd039d0
Ign http://archive.ubuntu.com trusty InRelease
Get:1 http://archive.ubuntu.com trusty-updates InRelease [56.4 kB]
Get:2 http://archive.ubuntu.com trusty-backports InRelease [65.9 kB]
Hit http://archive.ubuntu.com trusty Release.gpg
Get:3 http://archive.ubuntu.com trusty-updates/main amd64 Packages [1460 kB]
Get:4 https://esm.ubuntu.com trusty-infra-security InRelease
Get:5 http://security.ubuntu.com trusty-security InRelease [56.4 kB]
Get:6 https://esm.ubuntu.com trusty-infra-updates InRelease
Get:7 http://archive.ubuntu.com trusty-updates/restricted amd64 Packages [28.7 kB]
Get:8 http://archive.ubuntu.com trusty-updates/universe amd64 Packages [884 kB]
Get:9 https://esm.ubuntu.com trusty-infra-security/main amd64 Packages
Get:10 http://archive.ubuntu.com trusty-updates/multiverse amd64 Packages [21.7 kB]
Get:11 http://archive.ubuntu.com trusty-backports/main amd64 Packages [14.7 kB]
Get:12 http://archive.ubuntu.com trusty-backports/restricted amd64 Packages [40 B]
Get:13 http://archive.ubuntu.com trusty-backports/universe amd64 Packages [52.5 kB]
Get:14 http://archive.ubuntu.com trusty-backports/multiverse amd64 Packages [1392 B]
Get:15 https://esm.ubuntu.com trusty-infra-updates/main amd64 Packages
Hit http://archive.ubuntu.com trusty Release
Get:16 http://archive.ubuntu.com trusty/main amd64 Packages [1743 kB]
Get:17 http://archive.ubuntu.com trusty/restricted amd64 Packages [16.0 kB]
Get:18 http://archive.ubuntu.com trusty/universe amd64 Packages [7589 kB]
Get:19 http://security.ubuntu.com trusty-security/main amd64 Packages [877 kB]
Get:20 http://archive.ubuntu.com trusty/multiverse amd64 Packages [169 kB]
Get:21 http://security.ubuntu.com trusty-security/restricted amd64 Packages [24.6 kB]
Get:22 http://security.ubuntu.com trusty-security/universe amd64 Packages [490 kB]
Get:23 http://security.ubuntu.com trusty-security/multiverse amd64 Packages [6133 B]
Fetched 14.3 MB in 1s (12.3 MB/s)
Reading package lists...
Removing intermediate container 7ea2bdd039d0
 ---> 3a419c4f0f1e
Step 4/5 : ENTRYPOINT ["/bin/ping", "-c", "3"]
 ---> Running in b1f34358e3cf
Removing intermediate container b1f34358e3cf
 ---> 8fee0235c8ee
Step 5/5 : CMD ["localhost"]
 ---> Running in 95842833160e
Removing intermediate container 95842833160e
 ---> 91b72ef64a7e
Successfully built 91b72ef64a7e
Successfully tagged formation:1.0
```

* Recréer l'image associée à ce _Dockerfile_ en lui donnant un autre tag (par exemple `formation:2.0`). Observer le résultat.

On relance la création de l'image en changeant le tag.

```
florent@LAPTOP-S2TDFR7B:~/TP3$ docker build -t formation:2.0 .
Sending build context to Docker daemon  2.048kB
Step 1/5 : FROM ubuntu:trusty
 ---> 13b66b487594
Step 2/5 : LABEL maintainer=florent@example.com
 ---> Using cache
 ---> 55895539741d
Step 3/5 : RUN apt-get update
 ---> Using cache
 ---> 3a419c4f0f1e
Step 4/5 : ENTRYPOINT ["/bin/ping", "-c", "3"]
 ---> Using cache
 ---> 8fee0235c8ee
Step 5/5 : CMD ["localhost"]
 ---> Using cache
 ---> 91b72ef64a7e
Successfully built 91b72ef64a7e
Successfully tagged formation:2.0
```

Le cache de Docker est utilisé. On peut empêcher l'utilisation du cache avec l'option `--no-cache`.

* Consulter la liste des images sur la machines

```
florent@LAPTOP-S2TDFR7B:~/TP3$ docker image ls
REPOSITORY           TAG       IMAGE ID       CREATED              SIZE
formation            1.0       91b72ef64a7e   About a minute ago   211MB
formation            2.0       91b72ef64a7e   About a minute ago   211MB
```

Les deux images ont le même ID.

* Lancer un conteneur basé sur l'image `formation:1.0`

```
florent@LAPTOP-S2TDFR7B:~/TP3$ docker run formation:1.0
PING localhost (127.0.0.1) 56(84) bytes of data.
64 bytes from localhost (127.0.0.1): icmp_seq=1 ttl=64 time=0.081 ms
64 bytes from localhost (127.0.0.1): icmp_seq=2 ttl=64 time=0.046 ms
64 bytes from localhost (127.0.0.1): icmp_seq=3 ttl=64 time=0.039 ms

--- localhost ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2097ms
rtt min/avg/max/mdev = 0.039/0.055/0.081/0.019 ms
```

* Relancer un conteneur en indiquant une URL à la suite de la commande `docker run` (par exemple `"google.com"`)

```
florent@LAPTOP-S2TDFR7B:~/TP3$ docker run formation:1.0 google.com
PING google.com (216.58.214.174) 56(84) bytes of data.
64 bytes from par10s42-in-f14.1e100.net (216.58.214.174): icmp_seq=1 ttl=115 time=8.30 ms
64 bytes from par10s42-in-f14.1e100.net (216.58.214.174): icmp_seq=2 ttl=115 time=5.75 ms
64 bytes from par10s42-in-f14.1e100.net (216.58.214.174): icmp_seq=3 ttl=115 time=9.98 ms

--- google.com ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2002ms
rtt min/avg/max/mdev = 5.758/8.015/9.986/1.737 ms
```

Ici, on a changé le CMD pour mettre `google.com` à la place de `localhost`

* Relancer un conteneur en indiquant une autre commande à la suite de la commande `docker run` (par exemple `echo "Salut !"`). Corriger le problème pour que le `echo` puisse fonctionner.

```
florent@LAPTOP-S2TDFR7B:~/TP3$ docker run formation:1.0 echo "Salut !"
ping: unknown host echo
```

On continue d'exécuter au final l'_entrypoint_, c'est à dire `ping -c -3`. Il faudrait en fait modifier l'entrypoint avec l'option `--entrypoint`.

```
florent@LAPTOP-S2TDFR7B:~/TP3$ docker run --entrypoint echo formation:1.0 "Salut!"
Salut!
```

## 2) Conteneuriser une application

Soit l'application qu'on retrouve dans ce dépôt Git : `https://gitlab.com/formationdok/Express_Mongo`

Il s'agit d'une application Node.js qui fonctionne avec une base de données MongoDB.

On souhaite conteneuriser cette application.

* Cloner le repository Git de l'application en local sur sa machine avec `git clone https://gitlab.com/formationdok/Express_Mongo`

```
florent@LAPTOP-S2TDFR7B:~/TP3$ git clone https://gitlab.com/formationdok/Express_Mongo
Cloning into 'Express_Mongo'...
warning: redirecting to https://gitlab.com/formationdok/Express_Mongo.git/
remote: Enumerating objects: 17, done.
remote: Counting objects: 100% (17/17), done.
remote: Compressing objects: 100% (14/14), done.
remote: Total 17 (delta 0), reused 17 (delta 0), pack-reused 0
Unpacking objects: 100% (17/17), 41.35 KiB | 8.27 MiB/s, done.
florent@LAPTOP-S2TDFR7B:~/TP3$ ls
Express_Mongo
florent@LAPTOP-S2TDFR7B:~/TP3$ cd Express_Mongo/
florent@LAPTOP-S2TDFR7B:~/TP3/Express_Mongo$
```

* Aller dans le répertoire de l'application qui vient d'être téléchargé et écrire le Dockerfile afin de conteneuriser cette application. Quelques indications :
  * On utilisera de base une image `node:16`
  * Au niveau de l'image, on travaille dans le répertoire `/usr/src/app`
  * On définit la connexion à la base de données avec la variable d'environnement `MONGO_URI` (par défaut `mongodb://mongo:27017` si non renseigné)
  * On spécifie le port d'écoute de l'application avec la variable d'environnement `PORT` (par défaut `6060`)
  * La commande qui permet de télécharger les dépendances du projet : `npm ci`
  * La commande qui permet de lancer l'application : `npm start`

Voici le Dockerfile :

```
FROM node:16
LABEL maintainer=florent@example.com
ENV MONGO_URI=mongodb://mongo:27017
ENV PORT=6060
WORKDIR /usr/src/app
COPY . .
RUN npm ci
ENTRYPOINT ["npm", "start"]
```

Pour construire l'image : `docker build -t nodeapp:1.0 .`

* Essayer de lancer un conteneur de notre application en publiant le port 6060 avec un `docker run` et observer le résultat

```
florent@LAPTOP-S2TDFR7B:~/TP3/Express_Mongo$ docker run -p 6060:6060 nodeapp:1.0

> express_mongo@1.0.0 start
> node server.js

Server started on port 6060
getaddrinfo ENOTFOUND mongo
```

Notre conteneur se lance mais l'application plante car le serveur MongoDB n'est pas disponible.

* On va monter une petite infrastructure pour tester notre application en lançant un serveur MongoDB
  * Créer un réseau bridge `app`

```
florent@LAPTOP-S2TDFR7B:~/TP3/Express_Mongo$ docker network create -d bridge app
27547f98bff26a9d9829187675b60136ba66faa1f46fd82b58b42c76848d758b
```

```
florent@LAPTOP-S2TDFR7B:~/TP3/Express_Mongo$ docker network ls
NETWORK ID     NAME      DRIVER    SCOPE
27547f98bff2   app       bridge    local
```

  * Lancer un conteneur `mongo` basé sur une image `mongo:latest` sur ce réseau bridge `app`

```
florent@LAPTOP-S2TDFR7B:~/TP3/Express_Mongo$ docker run -d --net app --name mongo -d mongo
82500ed60cacf54dbf6a141a27f14dea5bd438cee4efd73f5a43d662b3c88390
```
  
  * Lancer un conteneur de notre application sur ce réseau bridge `app` avec en publiant le port 6060 vers le port 6060 du serveur Docker

```
florent@LAPTOP-S2TDFR7B:~/TP3/Express_Mongo$ docker run --net app -p 6060:6060 nodeapp:1.0
Server started on port 5000
MONGO DB Connected ...
```

  * Dans le navigateur, essayer d'aller sur `http://localhost:6060` et observer le résultat

On ouvre le navigateur et on teste l'URL.

* Supprimer le conteneur `mongo` et le conteneur de l'application

On commence à être habitués à ces commandes...

* Relancer un conteneur basé sur l'image `mongo` avec cette fois-ci `mongoapp` comme nom, toujours sur le réseau `app`

```
florent@LAPTOP-S2TDFR7B:~$ docker run -d --net app --name mongoapp -d mongo
7ee127d51f20362eaffba9c847506de721484a00e667e3675f925d7a3d775b53
```

* Relancer le conteneur de l'application, en mettant les bonnes variables d'environnement, pour qu'il écoute sur le port `6789`

```
florent@LAPTOP-S2TDFR7B:~$ docker run --net app -e PORT=6789 -e MONGO_URI=mongodb://mongoapp:27017 -p 6789:6789  nodeapp:1.0

> express_mongo@1.0.0 start
> node server.js

Server started on port 6789
MONGO DB Connected ...
```

* Supprimer le conteneur `mongoapp` et le conteneur de l'application

On commence à être vraiment habitués à ces commandes...

## 3) Implémenter un HEALTHCKECK

On va essayer d'implémenter un _health check_ pour notre application

Dans le répertoire de l'application, on trouve un fichier `healthcheck.js`. Lancer ce fichier permet de faire une simple requête HTTP sur _localhost_ pour retourner `0` si le code retour est 200, `1` sinon.

* Modifier le _Dockerfile_ de l'application pour lancer ce _healthcheck_ toutes les 15 secondes, après que le conteneur ait été lancé depuis 30 secondes. On règlera un _timeout_ de 2 secondes. On lancera la commande `node healthcheck.js`

Voici le _Dockerfile_. On note l'ajout de la clause `HEALTHCHECK`

```
FROM node:16
LABEL maintainer=florent@example.com
ENV MONGO_URI=mongodb://mongo:27017
ENV PORT=6060
WORKDIR /usr/src/app
COPY . .
RUN npm ci
HEALTHCHECK --interval=15s --start-period=30s --timeout=2s CMD node healthcheck.js
ENTRYPOINT ["npm", "start"]
```

* Reconstruire l'image

Pour construire l'image : `docker build -t nodeapp:2.0 .`

* Relancer l'application avec le serveur MongoDB associé

```
florent@LAPTOP-S2TDFR7B:~/TP3/Express_Mongo$ docker run -d --net app --name mongo -d mongo
4879040dea390693faf3245fb379333c0df3f65ee5f4019e848049dc84e72097
florent@LAPTOP-S2TDFR7B:~/TP3/Express_Mongo$ docker run -d --net app -p 6060:6060 nodeapp:2.0
7b81d88f0d4f07f8e236f3af303fd86132a91a24dbcbbc7e55b9ea3e159eb25d
florent@LAPTOP-S2TDFR7B:~/TP3/Express_Mongo$ docker logs 7b81d88

> express_mongo@1.0.0 start
> node server.js

Server started on port 6060
MONGO DB Connected ...
```

* Vérifier que le conteneur est en bonne santé

```
florent@LAPTOP-S2TDFR7B:~/TP3/Express_Mongo$ docker ps
CONTAINER ID   IMAGE         COMMAND                  CREATED              STATUS                    PORTS                                       NAMES
7b81d88f0d4f   nodeapp:2.0   "npm start"              42 seconds ago       Up 41 seconds (healthy)   0.0.0.0:6060->6060/tcp, :::6060->6060/tcp   awesome_bohr
4879040dea39   mongo         "docker-entrypoint.s…"   About a minute ago   Up About a minute         27017/tcp                                   mongo
```

Si on modifier le fichier `healthcheck.js` en changeant le code retour HTTP attendu (pour tester), on voit au bout d'un certain temps que le conteneur est marqué comme _unhealthy_. Même s'il est _unhealthy_, on constate que Docker continue de le faire fonctionner et il reste accessible.
