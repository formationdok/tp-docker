# TP2 - Gestion du réseau - Corrigé

## 1) Accès réseau **par défaut**
  * Lancer le conteneur _(lui donner comme nom : `archlinux-simple`)_
    * Verifier qu'il a accès au réseau, en totalité
      ```sh
      ~/formation/docker/tp2-reseau
      user@lx-5-35
      ✓ (0) $ docker run -it --name "archlinux-simple" --volume "$(pwd)"/data/:"/mnt/donnees" archlinux:base-20230528.0.154326
      [root@bc269c9e7a14 /]#
      ```
      <!-- `` -->
        - Maintenant on est dans le conteneur, attention !
          ```sh
          [root@bc269c9e7a14 /]# cd /mnt/donnees/
          [root@bc269c9e7a14 donnees]# curl -X 'GET' 'https://petstore.swagger.io/v2/pet/findByStatus?status=sold' -H 'accept: application/json' -o result.json
          [root@bc269c9e7a14 donnees]# ls
          result.json
          ```
          <!-- `` -->
        - là 3 choix :
          - ouverture du fichier directement dans firefox depuis l'interface graphique
          - sortie du conteneur pour revenir à l'hôte _(par ex : `CTRL+P` puis `CTRL+Q`)_
          - ouverture d'une autre console dans `~/formation/docker/tp2-reseau/`
            ```sh
            ~/formation/docker/tp2-reseau
            user@lx-5-35
            ✓ (0) $ ls data/
            result.json

            ~/formation/docker/tp2-reseau
            user@lx-5-35
            ✓ (0) $ firefox data/result.json &
            [1] 21752

            ```
            <!-- `` -->
              ![center](pictures/docker_tp2_reseau_json_firefox.png)

  * Créer un autre conteneur avec la même commande que précédemment, en changeant son nom _(ex: `archlinux-duplicate`)_
    ```sh
    ~/formation/docker/tp2-reseau
    user@lx-5-35
    ✓ (0) $ docker run -it --name "archlinux-duplicate" --volume "$(pwd)"/data/:"/mnt/donnees" archlinux:base-20230528.0.154326
    [root@bc269c9e7a14 /]#
    ```
    <!-- `` -->
    * Quelles sont leurs adresses IP ? Leurs hostnames ? Quel est leur type de réseau ?
      - dans les conteneurs
        - **archlinux-simple** (hostname : **bc269c9e7a14** _via la lecture du prompt_)
          ```sh
          [root@bc269c9e7a14 donnees]# ip addr show up | grep inet |  grep -v ":" | grep -v "127.0.0.1"
          inet 172.17.0.2/16 brd 172.17.255.255 scope global eth0
          ```
          <!-- `` -->
          &rarr; **172.17.0.2**

        - **archlinux-duplicate** (hostname : **117bc1c36262**)
          ```sh
          [root@117bc1c36262 /]# ip addr show up | grep inet |  grep -v ":" | grep -v "127.0.0.1"
          inet 172.17.0.3/16 brd 172.17.255.255 scope global eth0
          ```
          <!-- `` -->
          &rarr; **172.17.0.3**
      - sur le host
        ```sh
        ~/formation/docker/tp2-reseau
        user@lx-5-35
        ✓ (0) $ docker inspect archlinux-simple  | egrep "IPAddress|Hostname|bridge|NetworkMode" | grep -iv path
                    "NetworkMode": "default",
                    "Hostname": "bc269c9e7a14",
                    "SecondaryIPAddresses": null,
                    "IPAddress": "172.17.0.2",
                        "bridge": {
                            "IPAddress": "172.17.0.2",

        ~/formation/docker/tp2-reseau
        user@lx-5-35
        ✓ (0) $ docker inspect archlinux-duplicate  | egrep "IPAddress|Hostname|bridge|NetworkMode" | grep -iv path
                    "NetworkMode": "default",
                    "Hostname": "117bc1c36262",
                    "SecondaryIPAddresses": null,
                    "IPAddress": "172.17.0.3",
                        "bridge": {
                            "IPAddress": "172.17.0.3",
        ```
        <!-- `` -->
        &rarr; **réseau par défaut !**
    * Vérifier que **archlinux-duplicate** peut contacter **archlinux-simple** _et vice versa_
      * par IP &rarr; OK
      * par Hostname &rarr; KO _(pourquoi ?)_
        - depuis **archlinux-duplicate**
          ```sh
          [root@117bc1c36262 /]# ping -c 3 172.17.0.2
          PING 172.17.0.2 (172.17.0.2) 56(84) bytes of data.
          64 bytes from 172.17.0.2: icmp_seq=1 ttl=64 time=0.063 ms
          64 bytes from 172.17.0.2: icmp_seq=2 ttl=64 time=0.047 ms
          64 bytes from 172.17.0.2: icmp_seq=3 ttl=64 time=0.038 ms

          --- 172.17.0.2 ping statistics ---
          3 packets transmitted, 3 received, 0% packet loss, time 2041ms
          rtt min/avg/max/mdev = 0.038/0.049/0.063/0.010 ms

          [root@117bc1c36262 /]# ping -c 3 bc269c9e7a14
          ping: bc269c9e7a14: Name or service not known
          ```
          <!-- `` -->
        - depuis **archlinux-simple**
          ```sh
          [root@bc269c9e7a14 donnees]# ping -c 3 172.17.0.3
          PING 172.17.0.3 (172.17.0.3) 56(84) bytes of data.
          64 bytes from 172.17.0.3: icmp_seq=1 ttl=64 time=0.045 ms
          64 bytes from 172.17.0.3: icmp_seq=2 ttl=64 time=0.057 ms
          64 bytes from 172.17.0.3: icmp_seq=3 ttl=64 time=0.056 ms

          --- 172.17.0.3 ping statistics ---
          3 packets transmitted, 3 received, 0% packet loss, time 2038ms
          rtt min/avg/max/mdev = 0.045/0.052/0.057/0.005 ms


          [root@bc269c9e7a14 donnees]# ping -c 3 117bc1c36262
          ping: 117bc1c36262: Name or service not known
          ```
          <!-- `` -->
          &rarr; dans le mode **réseau par défaut**, il n'y a pas de résolution DNS automatique, il faut remplir manuellement le fichier hosts _(`/etc/hosts`)_ de chaque conteneur _([source](https://stackoverflow.com/a/48365699))_

  * Créer un 3e conteneur, toujours avec la même commande _(nom: `archlinux-bridge`)_, en précisant cette fois le réseau bridge par défaut et en donnant comme hostname `arch-host-bridge`
    ```sh
    ~/formation/docker/tp2-reseau
    user@lx-5-35
    ✓ (0) $ docker run -it --name "archlinux-bridge" --volume "$(pwd)"/data/:"/mnt/donnees" --net-alias "arch-host-bridge" --network "bridge" archlinux:base-20230528.0.154326
    [root@arch-host-bridge /]#
    ```
    <!-- `` -->
    * Vérifier que **archlinux-bridge** peut contacter **archlinux-simple** et **archlinux-duplicate**
      * par IP &rarr; OK
      * par Hostname &rarr; KO _(pourquoi ?)_
        ```sh
        [root@arch-host-bridge /]# ping -c 3 172.17.0.3
        PING 172.17.0.3 (172.17.0.3) 56(84) bytes of data.
        64 bytes from 172.17.0.3: icmp_seq=1 ttl=64 time=0.071 ms
        64 bytes from 172.17.0.3: icmp_seq=2 ttl=64 time=0.037 ms
        64 bytes from 172.17.0.3: icmp_seq=3 ttl=64 time=0.043 ms

        --- 172.17.0.3 ping statistics ---
        3 packets transmitted, 3 received, 0% packet loss, time 2040ms
        rtt min/avg/max/mdev = 0.037/0.050/0.071/0.014 ms


        [root@arch-host-bridge /]# ping -c 3 172.17.0.2
        PING 172.17.0.2 (172.17.0.2) 56(84) bytes of data.
        64 bytes from 172.17.0.2: icmp_seq=1 ttl=64 time=0.081 ms
        64 bytes from 172.17.0.2: icmp_seq=2 ttl=64 time=0.049 ms
        64 bytes from 172.17.0.2: icmp_seq=3 ttl=64 time=0.049 ms

        --- 172.17.0.2 ping statistics ---
        3 packets transmitted, 3 received, 0% packet loss, time 2087ms
        rtt min/avg/max/mdev = 0.049/0.059/0.081/0.015 ms


        [root@arch-host-bridge /]# ping -c 3 117bc1c36262
        ping: 117bc1c36262: Name or service not known

        [root@arch-host-bridge /]# ping -c 3 bc269c9e7a14
        ping: bc269c9e7a14: Name or service not known
        [root@arch-host-bridge /]#
        ```
        <!-- `` -->

## 2) Conteneur **sans accès réseau**
  * Lancer un conteneur sans accès réseau
    * nom &rarr; `archlinux-no-network`
    * Vérifier qu'il ne peut se connecter à rien

      ```sh
      ~/formation/docker/tp2-reseau
      user@lx-5-35
      ✓ (0) $ docker run -it --name "archlinux-no-network" --volume "$(pwd)"/data/:"/mnt/donnees" --net-alias "arch-no-net" --network "none" archlinux:base-20230528.0.154326

      [root@arch-no-net /]# cd /mnt/donnees/

      [root@arch-no-net donnees]# ls
      result.json

      [root@arch-no-net donnees]# curl -X 'GET' 'https://petstore.swagger.io/v2/pet/findByStatus?status=sold' -H 'accept: application/json' -o result-none.json
        % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                       Dload  Upload   Total   Spent    Left  Speed
        0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0curl: (6) Could not resolve host: petstore.swagger.io

      [root@arch-no-net donnees]# ls
      result.json
      ```
      <!-- `` -->

## 3) Conteneur réseau **bridge**
  * Créer un réseau bridge, nommé  **mon-bridge**
    ```sh
    ~/formation/docker/tp2-reseau
    user@lx-5-35
    ✓ (0) $ docker network create "mon-bridge"
    6d43e48007b4af75673eb947b96d63fa38e89762e3b3ee5fa7d95257d40ed529
    ```
    <!-- `` --> <br>
    * Lister les réseaux présents, vérifier la présence de **mon-bridge**
      ```sh
      ~/formation/docker/tp2-reseau
      user@lx-5-35
      ✓ (0) $ docker network ls
      NETWORK ID     NAME               DRIVER    SCOPE
      32db8a0ad320   bridge             bridge    local
      ee45f8318cbd   host               host      local
      6d43e48007b4   mon-bridge         bridge    local
      0cfb0d0dff8f   mon-bridge-perso   bridge    local
      92b4a1cf34d6   none               null      local
      ```
      <!-- `` --> <br>
    * Inspecter les informations de **mon-bridge**
      ```sh
      ✓ (0) $ docker network inspect mon-bridge
      [
          {
              "Name": "mon-bridge",
              "Id": "6d43e48007b4af75673eb947b96d63fa38e89762e3b3ee5fa7d95257d40ed529",
              "Created": "2023-06-01T13:36:39.140876408+02:00",
              "Scope": "local",
              "Driver": "bridge",
              "EnableIPv6": false,
              "IPAM": {
                  "Driver": "default",
                  "Options": {},
                  "Config": [
                      {
                          "Subnet": "172.19.0.0/16",
                          "Gateway": "172.19.0.1"
                      }
                  ]
              },
              "Internal": false,
              "Attachable": false,
              "Ingress": false,
              "ConfigFrom": {
                  "Network": ""
              },
              "ConfigOnly": false,
              "Containers": {},
              "Options": {},
              "Labels": {}
          }
      ]
      ```
      <!-- `` --> <br>
    * Lancer deux conteneurs avec ce driver
      * nom &rarr; `machine1`
      * nom &rarr; `machine2`
    * Inspecter de nouveau les informations de  **mon-bridge**
    * Vérifier que **machine1** et **machine2** peuvent communiquer ensemble _(via un ping par exemple)_
      * Ouvrir un deuxième terminal
        ```sh
        ~/formation/docker/tp2-reseau
        user@lx-5-38
        ✓ (0) $ docker run -it --name "machine1" --volume "$(pwd)"/data/:"/mnt/donnees" --net-alias "premiere-machine" --network "mon-bridge" archlinux:base-20230528.0.154326
        [root@premiere-machine /]# ping -c 3 seconde-machine
        PING seconde-machine (172.19.0.3) 56(84) bytes of data.
        64 bytes from machine2.mon-bridge (172.19.0.3): icmp_seq=1 ttl=64 time=0.086 ms
        64 bytes from machine2.mon-bridge (172.19.0.3): icmp_seq=2 ttl=64 time=0.051 ms
        64 bytes from machine2.mon-bridge (172.19.0.3): icmp_seq=3 ttl=64 time=0.052 ms

        --- seconde-machine ping statistics ---
        3 packets transmitted, 3 received, 0% packet loss, time 2081ms
        rtt min/avg/max/mdev = 0.051/0.063/0.086/0.016 ms
        ```
        <!-- `` --> <br>
      * Ouvrir un troisième terminal
        ```sh
        ~/formation/docker/tp2-reseau
        user@lx-5-38
        ✓ (0) $ docker run -it --name "machine2" --volume "$(pwd)"/data/:"/mnt/donnees" --net-alias "seconde-machine"  --network "mon-bridge" archlinux:base-20230528.0.154326
        [root@seconde-machine /]# ping -c 3 premiere-machine
        PING premiere-machine (172.19.0.2) 56(84) bytes of data.
        64 bytes from machine1.mon-bridge (172.19.0.2): icmp_seq=1 ttl=64 time=0.046 ms
        64 bytes from machine1.mon-bridge (172.19.0.2): icmp_seq=2 ttl=64 time=0.052 ms
        64 bytes from machine1.mon-bridge (172.19.0.2): icmp_seq=3 ttl=64 time=0.051 ms

        --- premiere-machine ping statistics ---
        3 packets transmitted, 3 received, 0% packet loss, time 2077ms
        rtt min/avg/max/mdev = 0.046/0.049/0.052/0.002 ms
        ```
        <!-- `` --> <br>

    * Supprimer  **mon-bridge**
      ```sh
      ## Revenir sur le 1er terminal
      ~/formation/docker
      user@lx-5-35
      ✓ (0) $ docker network rm mon-bridge
      Error response from daemon: error while removing network: network mon-bridge id 6d43e48007b4af75673eb947b96d63fa38e89762e3b3ee5fa7d95257d40ed529 has active endpoints
      ```
      <!-- `` -->
      &rarr; Impossible de supprimer le réseau si des conteneurs l'utilisent  <br>
      &rarr; Il faut d'abord les déconnecter
      ```sh
      ~/formation/docker/tp2-reseau
      user@lx-5-38
      ✓ (0) $ docker network disconnect -f mon-bridge machine1

      ~/formation/docker/tp2-reseau
      user@lx-5-38
      ✓ (0) $ docker network disconnect -f mon-bridge machine2

      ~/formation/docker/tp2-reseau
      user@lx-5-38
      ✓ (0) $ docker network remove mon-bridge
      mon-bridge
      ```
      <!-- `` --> <br>
    * Reconnecter **machine1** et **machine2** sur le bridge par défaut
      ```sh
      ~/formation/docker/tp2-reseau
      user@lx-5-38
      ✓ (0) $ docker network connect bridge machine1

      ~/formation/docker/tp2-reseau
      user@lx-5-38
      ✓ (0) $ docker network connect bridge machine2
      ```
      <!-- `` --> <br>
    * Vérifier que **machine1** et **machine2** ne peuvent communiquer ensemble que par IP _(via un ping par exemple)_
      * Terminal **machine1**
        ```sh
        [root@premiere-machine /]# ping -c 3 seconde-machine
        ping: seconde-machine: Name or service not known
        [root@premiere-machine /]# ip addr show | grep brd
            link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
            link/ether 02:42:ac:11:00:03 brd ff:ff:ff:ff:ff:ff link-netnsid 0
            inet 172.17.0.3/16 brd 172.17.255.255 scope global eth1
        [root@premiere-machine /]# ping -c 3 172.17.0.4
        PING 172.17.0.4 (172.17.0.4) 56(84) bytes of data.
        64 bytes from 172.17.0.4: icmp_seq=1 ttl=64 time=0.084 ms
        64 bytes from 172.17.0.4: icmp_seq=2 ttl=64 time=0.053 ms
        64 bytes from 172.17.0.4: icmp_seq=3 ttl=64 time=0.040 ms

        --- 172.17.0.4 ping statistics ---
        3 packets transmitted, 3 received, 0% packet loss, time 2043ms
        rtt min/avg/max/mdev = 0.040/0.059/0.084/0.018 ms
        ```
        <!-- `` --> <br>
      * Terminal **machine2**
        ```sh
        [root@seconde-machine /]# ping -c 3 premiere-machine
        ping: premiere-machine: Name or service not known
        [root@seconde-machine /]# ip addr show | grep brd
            link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
            link/ether 02:42:ac:11:00:04 brd ff:ff:ff:ff:ff:ff link-netnsid 0
            inet 172.17.0.4/16 brd 172.17.255.255 scope global eth1
        [root@seconde-machine /]# ping -c 3 172.17.0.3
        PING 172.17.0.3 (172.17.0.3) 56(84) bytes of data.
        64 bytes from 172.17.0.3: icmp_seq=1 ttl=64 time=0.055 ms
        64 bytes from 172.17.0.3: icmp_seq=2 ttl=64 time=0.065 ms
        64 bytes from 172.17.0.3: icmp_seq=3 ttl=64 time=0.052 ms

        --- 172.17.0.3 ping statistics ---
        3 packets transmitted, 3 received, 0% packet loss, time 2044ms
        rtt min/avg/max/mdev = 0.052/0.057/0.065/0.005 ms
        ```
        <!-- `` --> <br>

  * **Pour ceux qui ont fini, après validation du formateur**
    * Créer un réseau bridge personnalisé _(exemple de nom : `mon-bridge-perso`)_ puis créer un 4e conteneur, avec _presque_ la même commande que pour **archlinux-bridge** _(nom: `archlinux-bridge-perso`)_, en précisant toutefois le réseau **mon-bridge-perso**.
      ```sh
      ~/formation/docker/tp2-reseau
      user@lx-5-35
      ✓ (0) $ docker network create "mon-bridge-perso"
      0cfb0d0dff8fba93c69ac99c14fe38daf41243da686f40b73359d5dc2e396021

      ~/formation/docker/tp2-reseau
      user@lx-5-35
      ✓ (0) $ docker network ls
      NETWORK ID     NAME               DRIVER    SCOPE
      32db8a0ad320   bridge             bridge    local
      ee45f8318cbd   host               host      local
      0cfb0d0dff8f   mon-bridge-perso   bridge    local
      92b4a1cf34d6   none               null      local

      ~/formation/docker/tp2-reseau
      user@lx-5-35
      ✓ (0) $ docker run -it --name "archlinux-bridge-perso" --volume "$(pwd)"/data/:"/mnt/donnees" --net-alias "arch-perso" --network "mon-bridge-perso" archlinux:base-20230528.0.154326
      [root@arch-perso /]#
      ```
      <!-- `` -->

        * Vérifier que **archlinux-bridge-perso** ne peut PAS contacter **archlinux-simple**, **archlinux-duplicate** et **archlinux-bridge**
          ```sh
          [root@arch-perso /]# ping -c 3 172.17.0.3
          PING 172.17.0.3 (172.17.0.3) 56(84) bytes of data.

          --- 172.17.0.3 ping statistics ---
          3 packets transmitted, 0 received, 100% packet loss, time 2034ms

          [root@arch-perso /]# ping -c 3 172.17.0.2
          PING 172.17.0.2 (172.17.0.2) 56(84) bytes of data.

          --- 172.17.0.2 ping statistics ---
          3 packets transmitted, 0 received, 100% packet loss, time 2058ms

          [root@arch-perso /]# ping -c 3 172.17.0.4
          PING 172.17.0.4 (172.17.0.4) 56(84) bytes of data.

          --- 172.17.0.4 ping statistics ---
          3 packets transmitted, 0 received, 100% packet loss, time 2035ms
          ```
          <!-- `` -->

          * Connecter **archlinux-duplicate** au réseau **mon-bridge-perso**
            ```sh
            ~/formation/docker/tp2-reseau
            user@lx-5-35
            ✓ (0) $ docker network connect mon-bridge-perso archlinux-duplicate

            ~/formation/docker/tp2-reseau
            user@lx-5-35
            ✓ (0) $

            ~/formation/docker/tp2-reseau
            user@lx-5-35
            ✓ (0) $ docker inspect archlinux-duplicate  | egrep "IPAddress|Hostname|bridge|NetworkMode" | grep -iv path
                        "NetworkMode": "default",
                        "Hostname": "117bc1c36262",
                        "SecondaryIPAddresses": null,
                        "IPAddress": "172.17.0.3",
                            "bridge": {
                                "IPAddress": "172.17.0.3",
                            "mon-bridge-perso": {
                                "IPAddress": "172.18.0.3",
            ```
            <!-- `` -->

            * Vérifier que **archlinux-bridge-perso** peut contacter **archlinux-duplicate**
              * par IP &rarr; OK
              * par Hostname &rarr; OK _(pourquoi ?)_
              * quel est le nouvel hostname de **archlinux-duplicate** ?
                - sur **archlinux-bridge-duplicate**
                  ```sh
                  [root@117bc1c36262 /]# ping -c 3 arch-perso
                  PING arch-perso (172.18.0.2) 56(84) bytes of data.
                  64 bytes from archlinux-bridge-perso.mon-bridge-perso (172.18.0.2): icmp_seq=1 ttl=64 time=0.045 ms
                  64 bytes from archlinux-bridge-perso.mon-bridge-perso (172.18.0.2): icmp_seq=2 ttl=64 time=0.052 ms
                  64 bytes from archlinux-bridge-perso.mon-bridge-perso (172.18.0.2): icmp_seq=3 ttl=64 time=0.054 ms

                  --- arch-perso ping statistics ---
                  3 packets transmitted, 3 received, 0% packet loss, time 2063ms
                  rtt min/avg/max/mdev = 0.045/0.050/0.054/0.003 ms
                  ```
                  <!-- `` -->
                - sur **archlinux-bridge-perso**
                  ```sh
                  [root@arch-perso /]# ping -c 3 172.18.0.3
                  PING 172.18.0.3 (172.18.0.3) 56(84) bytes of data.
                  64 bytes from 172.18.0.3: icmp_seq=1 ttl=64 time=0.052 ms
                  64 bytes from 172.18.0.3: icmp_seq=2 ttl=64 time=0.053 ms
                  64 bytes from 172.18.0.3: icmp_seq=3 ttl=64 time=0.053 ms

                  --- 172.18.0.3 ping statistics ---
                  3 packets transmitted, 3 received, 0% packet loss, time 2070ms
                  rtt min/avg/max/mdev = 0.052/0.052/0.053/0.000 ms

                  [root@arch-perso /]# ping -c 3  117bc1c36262
                  PING 117bc1c36262 (172.18.0.3) 56(84) bytes of data.
                  64 bytes from archlinux-duplicate.mon-bridge-perso (172.18.0.3): icmp_seq=1 ttl=64 time=0.084 ms
                  64 bytes from archlinux-duplicate.mon-bridge-perso (172.18.0.3): icmp_seq=2 ttl=64 time=0.061 ms
                  64 bytes from archlinux-duplicate.mon-bridge-perso (172.18.0.3): icmp_seq=3 ttl=64 time=0.060 ms

                  --- 117bc1c36262 ping statistics ---
                  3 packets transmitted, 3 received, 0% packet loss, time 2058ms
                  rtt min/avg/max/mdev = 0.060/0.068/0.084/0.011 ms
                  ```
                  <!-- `` -->
                  &rarr; nouvel hostname : **archlinux-duplicate.mon-bridge-perso** => cette fois on a la résolution DNS automatique !

## 4) Conteneur réseau **host**
  * Sur l'hote : lancer la commande `ip addr show eth0`. Sauvegarder le résultat dans un fichier texte
  * Lancer un conteneur en mode **host**, nommé `archlinux-host`
    * Se placer dans le répertoire `/mnt/donnees/`
    * Vérifier que la commande `ip addr show eth0` renvoie le même résultat que celle lancée sur l'hôte
      ```sh
      ~/formation/docker/tp2-reseau
      user@lx-5-35
      ✓ (0) $ ip addr show eth0
      2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
          link/ether 04:0e:3c:95:f4:5c brd ff:ff:ff:ff:ff:ff
          altname enp2s0
          inet 192.168.5.35/24 brd 192.168.5.255 scope global noprefixroute eth0
             valid_lft forever preferred_lft forever
          inet6 fe80::60e:3cff:fe95:f45c/64 scope link
             valid_lft forever preferred_lft forever

      ~/formation/docker/tp2-reseau
      user@lx-5-35
      ✓ (0) $ ip addr show eth0 > eth0-hote.txt

      ~/formation/docker/tp2-reseau
      user@lx-5-35
      ✓ (0) $ sudo mv eth0-hote.txt data/
      [sudo] Mot de passe de user :

      ~/formation/docker/tp2-reseau
      user@lx-5-35
      ✓ (0) $ docker run -it --name "archlinux-host" --volume "$(pwd)"/data/:"/mnt/donnees" --hostname "arch-host" --network "host" archlinux:base-20230528.0.154326
      ```

      ```sh
      [root@arch-host /]# cd /mnt/donnees/

      [root@arch-host donnees]# ls
      eth0-hote.txt  result.json

      [root@arch-host donnees]# ip addr show eth0 > eth0-conteneur.txt

      [root@arch-host donnees]# exit
      exit
      ```

      ```sh
      ~/formation/docker/tp2-reseau
      user@lx-5-35
      ✓ (0) $ diff data/eth0-hote.txt data/eth0-conteneur.txt

      mercredi 31 mai 2023 21:33:54
      ~/formation/docker/tp2-reseau
      user@lx-5-35
      ✓ (0) $ cat data/eth0-conteneur.txt
      2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
          link/ether 04:0e:3c:95:f4:5c brd ff:ff:ff:ff:ff:ff
          altname enp2s0
          inet 192.168.5.35/24 brd 192.168.5.255 scope global noprefixroute eth0
             valid_lft forever preferred_lft forever
          inet6 fe80::60e:3cff:fe95:f45c/64 scope link
             valid_lft forever preferred_lft forever
      ```
      <!-- `` -->
      &rarr; dans le mode **host**, le conteneur partage bien le même réseau que celui de l'hôte !


[Source](https://devopssec.fr/article/fonctionnement-manipulation-reseau-docker)
