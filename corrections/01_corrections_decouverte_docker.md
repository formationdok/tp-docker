# TP1 - Découverte de Docker - Corrigé

## 1) Manipulation des commandes

  - Afficher la version de docker
    ```sh
    $ docker --version

    Docker version 20.10.12, build e91ed57
    ```
    <!-- `` -->
  - Afficher l’aide
    ```sh
    $ docker --help

    Usage:  docker [OPTIONS] COMMAND

    A self-sufficient runtime for containers

    Options:
          --config string      Location of client config files (default
      ...
      -v, --version            Print version information and quit

    Management Commands:
      app*        Docker App (Docker Inc., v0.9.1-beta3)
      ...
      volume      Manage volumes

    Commands:
      attach      Attach local standard input, output, and error streams to a running container
      ...
      wait        Block until one or more containers stop, then print their exit codes

    Run ’docker COMMAND --help’ for more information on a command.

    To get more help with docker, check out our guides at https://docs.docker.com/go/guides/
    ```
    <!-- `` -->

## 2) Lancement du 1er conteneur

  - Lister les images déjà téléchargées. Vérifier qu’il n’en existe aucune
    ```sh
    $ docker images

    REPOSITORY   TAG       IMAGE ID   CREATED   SIZE
    ```
    <!-- `` -->
  - sinon les supprimer
    ```sh
    docker image rm hello-world
    ```
    <!-- `` -->

  - Lancer le conteneur `hello-world`. Regarder et interpréter ce qu’il se passe
    ```sh
    $ docker run hello-world

    Unable to find image ’hello-world:latest’ locally        # impossible de trouver l’image en local
    latest: Pulling from library/hello-world        # récupération depuis docker hub
    2db29710123e: Pull complete
    Digest: sha256:8be990ef2aeb16dbcb9271ddfe2610fa6658d13f6dfb8bc72074cc1ca36966a7
    Status: Downloaded newer image for hello-world:latest


    Hello from Docker.        # exécution
    This message shows that your installation appears to be working correctly.

    To generate this message, Docker took the following steps:
     1. The Docker client contacted the Docker daemon.
     2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
        (amd64)
     3. The Docker daemon created a new container from that image which runs the
        executable that produces the output you are currently reading.
     4. The Docker daemon streamed that output to the Docker client, which sent it
        to your terminal.

    To try something more ambitious, you can run an Ubuntu container with:
     $ docker run -it ubuntu bash

    Share images, automate workflows, and more with a free Docker ID:
     https://hub.docker.com/

    For more examples and ideas, visit:
     https://docs.docker.com/get-started/
    ```
    <!-- `` -->

  - Lister les images déjà téléchargées. Vérifier qu’il en existe une :
    ```sh
    $ docker images
    REPOSITORY    TAG       IMAGE ID       CREATED        SIZE
    hello-world   latest    feb5d9fea6a5   5 months ago   13.3kB
    ```
    <!-- `` -->

  - Afficher les informations détaillées de l’image
    ```sh
     $ docker image inspect hello-world
    ```
    ```json
    [
        {
            "Id": "sha256:feb5d9fea6a5e9606aa995e879d862b825965ba48de054caab5ef356dc6b3412",
            "RepoTags": [
                "hello-world:latest"
            ],
            "RepoDigests": [
                "hello-world@sha256:97a379f4f88575512824f3b352bc03cd75e239179eea0fecc38e597b2209f49a"
            ],
            "Parent": "",
            "Comment": "",
            "Created": "2021-09-23T23:47:57.442225064Z",
            "Container": "8746661ca3c2f215da94e6d3f7dfdcafaff5ec0b21c9aff6af3dc379a82fbc72",
            "ContainerConfig": {
              [...]
            },
              [...]
            "Metadata": {
                "LastTagTime": "0001-01-01T00:00:00Z"
            }
        }
    ]
    ```
    <!-- `` -->



  - Relancer le conteneur `hello-world`. Regarder et interpréter ce qu’il se passe
    ```sh
    $ docker run hello-world

    Hello from Docker!
    This message shows that your installation appears to be working correctly.

    To generate this message, Docker took the following steps:
     1. The Docker client contacted the Docker daemon.
     2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
        (amd64)
     3. The Docker daemon created a new container from that image which runs the
        executable that produces the output you are currently reading.
     4. The Docker daemon streamed that output to the Docker client, which sent it
        to your terminal.

    To try something more ambitious, you can run an Ubuntu container with:
     $ docker run -it ubuntu bash

    Share images, automate workflows, and more with a free Docker ID:
     https://hub.docker.com/

    For more examples and ideas, visit:
     https://docs.docker.com/get-started/
    ```
    <!-- `` -->
      &rarr; l'image n'est plus téléchargée depuis le  DockerHub vu qu'elle est maintenant en local !

## 3) Lancement plus avancé

  - Chercher sur docker hub un conteneur contenant `apache`
    ```sh
    $ docker search apache


    NAME                               DESCRIPTION                                     STARS     OFFICIAL   AUTOMATED
    httpd                              The Apache HTTP Server Project                  3911      [OK]
    tomcat                             Apache Tomcat is an open source implementati…   3274      [OK]
    cassandra                          Apache Cassandra is an open-source distribut…   1355      [OK]
    maven                              Apache Maven is a software project managemen…   1326      [OK]
     [...]
    ```

  - Récupérer le conteneur choisi depuis le docker hub
    ```sh
    $ docker image pull httpd

    Using default tag: latest
    latest: Pulling from library/httpd
    f7a1c6dad281: Pull complete
    f18d7c6e023b: Pull complete
    bf06bcf4b8a8: Pull complete
    4566427976c4: Pull complete
    70a943c2d5bb: Pull complete
    Digest: sha256:b7907df5e39a98a087dec5e191e6624854844bc8d0202307428dd90b38c10140
    Status: Downloaded newer image for httpd:latest
    docker.io/library/httpd:latest
    ```
    <!-- `` -->


  - Le lancer en mode détaché
    ```sh
    $ docker container run -d httpd

    7a9ced1689286c48a3c443162023a43afd6ea91267e3e9459a22bdedf6c40b88
    ```
    <!-- `` -->

  - Vérifier la liste des conteneurs lancés
    ```sh
    $ docker container ls -a
    CONTAINER ID   IMAGE         COMMAND              CREATED              STATUS                         PORTS     NAMES
    7a9ced168928   httpd         "httpd-foreground"   About a minute ago   Up About a minute              80/tcp    suspicious_wilson
    fd42ecfad970   hello-world   "/hello"             About an hour ago    Exited (0) About an hour ago             clever_mayer
    deb369bfcc2e   hello-world   "/hello"             About an hour ago    Exited (0) About an hour ago             reverent_torvalds
    ```
    <!-- `` -->

  - Arreter le conteneur apache, puis le supprimer
    ```sh
    $ docker container stop suspicious_wilson && docker container rm suspicious_wilson
    $ docker ps -a

    CONTAINER ID   IMAGE         COMMAND    CREATED       STATUS                   PORTS     NAMES
    deb369bfcc2e   hello-world   "/hello"   2 hours ago   Exited (0) 2 hours ago             reverent_torvalds

    ```
    <!-- `` -->

  - Le recréer en lui donnant comme nom `server-web`
    ```sh
    $ docker run -d --name "server-web" -p 80:80 httpd

    10e4a511ab3302b09e535456acaabc67c98f531c65fd8f111ad54c9bd49c432e
    ```
    <!-- `` -->

  - Vérifier que celui-ci est bien lancé en allant dans un navigateur : [http://localhost](http://localhost)

  - Le supprimer : `docker container stop server-web && docker container rm server-web`

## 4) Mode interactif

  - Exécuter docker pour obtenir un conteneur avec archlinux, en mode attaché et interactif.
    ```sh
    $ docker search arch

    NAME                              DESCRIPTION                                     STARS     OFFICIAL   AUTOMATED
    couchbase                         Couchbase Server is a NoSQL document databas…   869       [OK]
    archlinux                         Arch Linux is a simple, lightweight Linux di…   325       [OK]
    clearlinux                        Official docker build of Clear Linux OS for …   143       [OK]
    neo4j/neo4j-arm64-experimental    Experimental Neo4j images for the ARM64 arch…   4
    ...


    $ docker run -it archlinux

    Unable to find image ’archlinux:latest’ locally
    latest: Pulling from library/archlinux
    753d41b52f65: Pull complete
    8efbae8417aa: Pull complete
    Digest: sha256:21995dec139a4ebaa2919ddddc0cd2f84a0ca2f9a58722333d53bcf460f9a4ee
    Status: Downloaded newer image for archlinux:latest
    [root@7e553a629307 /]#

    ```
    <!-- `` -->

  - Lister le répertoire courant
    ```sh
    [root@7e553a629307 /]# ls
    bin   dev  home  lib64    opt   root  sbin  sys  usr
    boot  etc  lib     mnt    proc  run   srv   tmp  var
    ```
    <!-- `` -->

  - Obtenir la version de la distribution
    ```sh
    [root@7e553a629307 /]# cat /etc/os-release
    NAME="Arch Linux"
    PRETTY_NAME="Arch Linux"
    ID=arch
    BUILD_ID=rolling
    ANSI_COLOR="38;2;23;147;209"
    HOME_URL="https://archlinux.org/"
    DOCUMENTATION_URL="https://wiki.archlinux.org/"
    SUPPORT_URL="https://bbs.archlinux.org/"
    BUG_REPORT_URL="https://bugs.archlinux.org/"
    LOGO=archlinux-logo


    [root@7e553a629307 /]# cat /etc/issue
    Arch Linux \r (\l)
    ```
    <!-- `` -->

  - Vérifier que la mise à jour de debian ne fonctionne pas, mais celle d’Archlinux oui
    ```sh
    [root@7e553a629307 /]# yum update

    bash: yum: command not found

    [root@7e553a629307 /]# apt update

    bash: apt: command not found
    ```

    ```sh
    [root@7e553a629307 /]# pacman  -Syu

    :: Synchronizing package databases...
     core downloading...
     extra downloading...
     community downloading...
    :: Starting full system upgrade...
    resolving dependencies...
    looking for conflicting packages...

    warning: insufficient columns available for table display
    Packages (7) ca-certificates-mozilla-3.76-1  curl-7.81.0-3  dbus-1.14.0-1
                 gcc-libs-11.2.0-4  gettext-0.21-2  glib2-2.70.4-2
                 pacman-mirrorlist-20220227-1

    Total Download Size:    36.92 MiB
    Total Installed Size:  149.54 MiB
    Net Upgrade Size:       -0.73 MiB

    :: Proceed with installation? [Y/n]
    ```
    <!-- `` -->



  - Vérifier les valeurs des variables d’environnement suivantes : ${HOME}, ${HOSTNAME}, ${JAVA_HOME}
    ```sh
    $ echo ${HOME}
    /root

    $ echo ${HOSTNAME}
    a3d6c853efda


    $ echo ${JAVA_HOME}

    ```
    <!-- `` -->

  - Sortir du conteneur
    ```sh
    [root@7e553a629307 /]# exit
    exit

    osboxes@formation:~$ docker ps
    CONTAINER ID   IMAGE         COMMAND              CREATED          STATUS                     PORTS     NAMES

    osboxes@formation:~$ docker ps -a
    CONTAINER ID   IMAGE         COMMAND              CREATED          STATUS                     PORTS     NAMES
    7e553a629307   archlinux     "/usr/bin/bash"      12 minutes ago   Exited (1) 9 seconds ago             eager_galileo
    deb369bfcc2e   hello-world   "/hello"             2 hours ago      Exited (0) 2 hours ago               reverent_torvalds


    ```
    <!-- `` -->

## 5) Variables d’environnement

  - Recréer le conteneur archlinux en changeant les valeurs des variables d’environnement :
    - HOME => /var
    - HOSTNAME => hello-arch
    - JAVA_HOME => /opt/openjdk11

    ```sh
    $ docker rm eager_galileo
    $ docker run -it -e HOSTNAME="hello-arch" --env JAVA_HOME="/opt/openjdk11" --env HOME=/var archlinux

    [root@9dd6aac65a2d /]# echo ${HOME}
    /var

    [root@9dd6aac65a2d /]# echo ${HOSTNAME}
    hello-arch

    [root@9dd6aac65a2d /]# echo ${JAVA_HOME}
    /opt/openjdk11

    ```
    <!-- `` -->

## 6) Gestion des données
  - Créer un ou deux fichiers dans le $HOME puis sortir du conteneur
    ```sh
    [root@54ff904fec58 ~]# cd $HOME
    [root@54ff904fec58 ~]# touch toto.txt && echo "hello arch" > hello.md

    [root@54ff904fec58 ~]# ls
    hello.md  toto.txt

    [root@54ff904fec58 ~]# cat hello.md
    hello arch

    [root@54ff904fec58 ~]# exit

    ```
    <!-- `` -->
  - Revenir dessus, vérifier que les fichiers ont disparu
    ```sh
    $ docker ps -a
    CONTAINER ID   IMAGE         COMMAND              CREATED        STATUS                        PORTS     NAMES
    7e553a629307   archlinux     "/usr/bin/bash"      8 hours ago    Exited (130) 36 minutes ago             eager_galileo
    10e4a511ab33   httpd         "httpd-foreground"   9 hours ago    Up 9 hours                    80/tcp    server-web
    deb369bfcc2e   hello-world   "/hello"             10 hours ago   Exited (0) 10 hours ago                 reverent_torvalds

    $ docker run -it -e HOSTNAME="hello-arch" --env JAVA_HOME="/opt/openjdk11" --env HOME=/var archlinux

    [root@7e553a629307 /]# cd $HOME
    [root@7e553a629307 ~]# ls
    [root@7e553a629307 ~]# exit

    $ docker start myarch && docker attach eager_galileo

    [root@7e553a629307 /]# cd $HOME
    [root@7e553a629307 ~]# ls
    hello.md  toto.txt

    [root@7e553a629307 ~]# exit

    ```
    <!-- `` -->


## 7) Volumes

  - Créer un volume entre votre ordinateur et le conteneur
    ```sh
    $ docker volume create arch-data
    arch-data

    $ docker volume ls
    DRIVER    VOLUME NAME
    local     arch-data


    $ docker volume inspect arch-data
    [
        {
            "CreatedAt": "2022-03-06T20:23:33-05:00",
            "Driver": "local",
            "Labels": {},
            "Mountpoint": "/var/lib/docker/volumes/arch-data/_data",
            "Name": "arch-data",
            "Options": {},
            "Scope": "local"
        }
    ]


    $ docker run --name arch-with-vol -it -v arch-data:/mnt/arch/data archlinux

    [root@dfa2d2680470 /]# cd /mnt/arch/data
    [root@dfa2d2680470 data]# touch volume-arch.txt && echo "test volume arch" > test.md

    [root@dfa2d2680470 data]# ls
    test.md  volume-arch.txt

    [root@dfa2d2680470 data]# cat test.md
    test volume arch

    [root@dfa2d2680470 data]# exit
    ```
    <!-- `` -->
  - Vérification de la persistance des données
    ```sh
    $ docker start -i arch-with-vol

    [root@dfa2d2680470 /]# cd /mnt/arch/data
    [root@dfa2d2680470 data]# ls
    test.md  volume-arch.txt

    [root@dfa2d2680470 data]# exit
    ```
    <!-- `` -->


  - Association du volume en lecture seule à un conteneur debian
    ```sh
    $ docker run --name debian-with-vol-ro -it -v arch-data:/mnt/debian/data:ro debian

    Unable to find image ’debian:latest’ locally
    latest: Pulling from library/debian
    e4d61adff207: Already exists
    Digest: sha256:10b622c6cf6daa0a295be74c0e412ed20e10f91ae4c6f3ce6ff0c9c04f77cbf6
    Status: Downloaded newer image for debian:latest


    [root@7e553a629307 /]# cd /mnt/debian/data

    root@4b5e3df2a112:/mnt/debian/data# ls
    test.md  volume-arch.txt

    root@4b5e3df2a112:/mnt/debian/data# touch nouveau-fichier-debian.txt
    touch: cannot touch ’nouveau-fichier-debian.txt’: Read-only file system

    root@4b5e3df2a112:/mnt/debian/data# ls
    test.md  volume-arch.txt

    root@4b5e3df2a112:/mnt/debian/data# exit
    ```
    <!-- `` -->

  - Retour sur le conteneur archlinux
    ```sh
    $ docker start -i arch-with-vol

    [root@dfa2d2680470 /]# cd /mnt/arch/data

    [root@dfa2d2680470 data]# ls
    test.md  volume-arch.txt

    [root@dfa2d2680470 data]# exit
    ```
    <!-- `` -->

  - Suppression du volume
    ```sh
    $ docker volume rm arch-data

    Error response from daemon: remove arch-data: volume is in use - [dfa2d2680470aca34d0afcf4c8f6355807d46fc971ea346afdcf894e1b785904, 4b5e3df2a112cdc4cc6cbcc5ff38cdabb52b3f46d03f54dbead5ef1e8d4de7bb]

    $ docker rm debian-with-vol-ro arch-with-vol
    debian-with-vol-ro
    arch-with-vol

    $ docker volume rm arch-data
    arch-data
    ```
    <!-- `` -->

## 8) Bind mounts

  - Associer un répertoire entre l'ordinateur hôte et le conteneur archlinux
    ```sh
    $ mkdir -p share-dir
    $ echo "fichier from host" > share-dir/file-host.md

    $ ls share-dir
    file-host.md

    $ cat share-dir/file-host.md
    fichier from host

    $ docker run -it --name arch-with-bind -v share-dir:/mnt/arch/share archlinux

    [root@735fb8da1c74 /]# cd /mnt/arch/share
    [root@735fb8da1c74 share]# ls

    [root@735fb8da1c74 share]# exit
    ```
    <!-- `` -->

     &rarr; Surprise, on n’obtient rien !

    - Cela vient de la syntaxe de l’option `-v`, on n’a pas pris le **chemin absolu** :
      ```sh
      $ docker rename arch-with-bind arch-with-bind-bad
      $ docker run -it --name arch-with-bind -v "$(pwd)"/share-dir:/mnt/arch/share archlinux

      [root@91ed6768be55 /]# cd /mnt/arch/share
      [root@91ed6768be55 share]# ls
      file-host.md

      [root@91ed6768be55 share]# cat file-host.md
      fichier from host

      [root@91ed6768be55 share]# echo "fichier from container" > file-container.md
      [root@91ed6768be55 share]# ls
      file-container.md  file-host.md

      [root@91ed6768be55 share]# cat file-container.md
      fichier from container

      [root@91ed6768be55 share]# exit

      $ ls share-dir
      file-container.md  file-host.md

      $ cat share-dir/file-container.md
      fichier from container
      ```
      <!-- `` -->

  - Créer un conteneur  **alpine**, associer ce répertoire et vérifier que les fichiers sont bien présents
      - utiliser la syntaxe `--mount` au lieu de `-v`
          ```sh
          $ docker run -it                                                      \
              --name alpine-with-bind                                           \
              --mount type=bind,source="$(pwd)"/share-dir,target=/mnt/data      \
                  alpine:latest

          / # cd /mnt/data
          /mnt/data# ls
          file-container.md  file-host.md

          /mnt/data# cat file-container.md
          fichier from container

          /mnt/data# cat file-host.md
          fichier from host

          /mnt/data# exit
          ```
          <!-- `` -->
