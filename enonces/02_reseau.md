# TP2 - Gestion du réseau

## Quelques astuces :

  - Pour l'ensemble du TP on utilisera l'image  **`archlinux:base-20230528.0.154326`** :
    - en mode interactif
    - avec un bind mount `~/formation/docker/tp2-reseau/data/` &rarr; `/mnt/donnees/`
  - Commande pour contacter une machine : `ping -c 3 <ip-ou-hotname>`
  - Ordre des exercices : 2, 4, 1, 3 _(il s'agit juste d'une suggestion)_
  - Ajouter un alias réseau sur un conteneur : `docker run --net-alias "mon-nom-de-machine-sur-le-reseau" <image>`
  - Créer une arborescence linux : `mkdir -p "/path/to/directory/"`

## 1) Accès réseau **par défaut**
  * Lancer le conteneur _(lui donner comme nom : `archlinux-simple`)_
    * Se placer dans le répertoire `/mnt/donnees/`
    * Verifier qu'il a accès au réseau, en totalité
      * par ex :
        ```sh
        curl -X 'GET' 'https://petstore.swagger.io/v2/pet/findByStatus?status=sold' -H 'accept: application/json' -o result.json
        ```
        <!-- `` --> 
        &rarr; visualiser le fichier téléchargé via firefox
  * Créer un autre conteneur avec la même commande que précédemment, en changeant son nom _(ex: `archlinux-duplicate`)_
    * Quelles sont leurs adresses IP ? Leurs hostnames ? Quel est leur type de réseau ?
    * Vérifier que **archlinux-duplicate** peut contacter **archlinux-simple** _et vice versa_
      * par IP &rarr; OK
      * par Hostname &rarr; KO _(pourquoi ?)_
  * Créer un 3e conteneur, toujours avec la même commande _(nom: `archlinux-bridge`)_, en précisant cette fois le réseau bridge par défaut et en donnant comme hostname `arch-host-bridge`
    * Vérifier que **archlinux-bridge** peut contacter **archlinux-simple** et **archlinux-duplicate**
      * par IP &rarr; OK
      * par Hostname &rarr; KO

## 2) Conteneur **sans accès réseau**
  * Lancer un conteneur sans accès réseau
    * nom &rarr; `archlinux-no-network`
    * Se placer dans le répertoire `/mnt/donnees/`
    * Vérifier qu'il ne peut se connecter à rien
      * par ex, la commande `curl -X 'GET' 'https://petstore.swagger.io/v2/pet/findByStatus?status=sold' -H 'accept: application/json' -o result-none.json` retourne une erreur

## 3) Conteneur réseau **bridge**
  * Créer un réseau bridge, nommé  **mon-bridge**
    * Lister les réseaux présents, vérifier la présence de **mon-bridge**
    * Inspecter les informations de **mon-bridge**
    * Lancer deux conteneurs avec ce driver
      * nom &rarr; `machine1`
      * nom &rarr; `machine2`
    * Inspecter de nouveau les informations de  **mon-bridge**
    * Vérifier que **machine1** et **machine2** peuvent communiquer ensemble _(via un ping par exemple)_
    * Supprimer  **mon-bridge**
    * Reconnecter **machine1** et **machine2** sur le bridge par défaut
    * Vérifier que **machine1** et **machine2** ne peuvent communiquer ensemble que par IP _(via un ping par exemple)_

  * **Pour ceux qui ont fini, après validation du formateur**
    * Créer un autre réseau bridge personnalisé _(exemple de nom : `mon-bridge-perso`)_ puis créer un 4e conteneur, avec _presque_ la même commande que pour **archlinux-bridge** _(nom: `archlinux-bridge-perso`)_, en précisant toutefois le réseau **mon-bridge-perso**. Ne pas oublier de changer le hostname, évidemment !
      * Vérifier que **archlinux-bridge-perso** ne peut PAS contacter **archlinux-simple**, **archlinux-duplicate** et **archlinux-bridge**
        * Connecter **archlinux-duplicate** au réseau **mon-bridge-perso**
          * Vérifier que **archlinux-bridge-perso** peut contacter **archlinux-duplicate**
            * par IP &rarr; OK
            * par Hostname &rarr; OK _(pourquoi ?)_
            * quel est le nouvel hostname de **archlinux-duplicate** ?

## 4) Conteneur réseau **host**
  * Sur l'hote : lancer la commande `ip addr show eth0`. Sauvegarder le résultat dans un fichier texte
  * Lancer un conteneur en mode **host**, nommé `archlinux-host`
    * Se placer dans le répertoire `/mnt/donnees/`
    * Vérifier que la commande `ip addr show eth0` renvoie le même résultat que celle lancée sur l'hote <br><br>



