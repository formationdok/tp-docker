# README

## Quelques Astuces :

  - Créer pour chaque TP un dossier sur le disque  (par exemple `~/formation/docker/tp1-découverte/` pour le TP1)
  - Ne restez pas coincés longtemps _(après avoir cherché un minimum)_, sollicitez le formateur !
  - Pour sortir d'un conteneur sans l'arrêter : `CTRL+P` puis `CTRL+Q`
  - Quelques commandes docker _(n'hésitez pas à consulter le cours ou le formateur ou encore Google)_:
    - `docker container` : gère les conteneurs
      - `run` : crée démarre un nouveau conteneur
      - `start` : démarre un ou plusieurs conteneurs déjà créé
      - `attach` : se connecte à un conteneur en cours d'exécution
      - `stop` : arrête un ou plusieurs conteneurs
      - `rm` : supprime un ou plusieurs conteneurs arrêtés
      - `inspect` : affiche des infos détaillées sur une ou plusieurs conteneurs
      - `exec` : exécute un programme dans le conteneur
      - `ls` : liste les conteneurs actifs _(option `-a` pour tous les conteneurs créés)_ 
    - `docker image` : gère les images
      - `pull` : récupère une image depuis le registre
      - `rm` : supprime une ou plusieurs images _(si aucun conteneur ne l'utilise)_
      - `inspect` : affiche des infos détaillées sur une ou plusieurs images
    - `docker network`: gère les réseaux
    - `docker volume` : gère les volumes
  - Quelques raccourcis
    - `docker ps` : liste les conteneurs actifs _(option `-a` pour tous les conteneurs créés)_ 
    - `docker images` : liste les images déjà téléchargées
    - `docker rm` : supprime un ou plusieurs conteneurs
    - `docker rmi` : supprime une ou plusieurs images _([source](https://www.digitalocean.com/community/tutorials/how-to-remove-docker-images-containers-and-volumes-fr))_
