# TP3 - Dockerfile

## 1) Un premier Dockerfile

Soit le _Dockerfile_ suivant :

```
FROM ubuntu:trusty
LABEL maintainer=florent@example.com
RUN apt-get update
ENTRYPOINT ["/bin/ping", "-c", "3"]
CMD ["localhost"]
```

* Créer l'image associée à ce _Dockerfile_ en n'oubliant pas de la tagger (par exemple `formation:1.0`)
* Recréer l'image associée à ce _Dockerfile_ en lui donnant un autre tag (par exemple `formation:2.0`). Observer le résultat.
* Consulter la liste des images sur la machines
* Lancer un conteneur basé sur l'image `formation:1.0`
* Relancer un conteneur en indiquant une URL à la suite de la commande `docker run` (par exemple `"google.com"`)
* Relancer un conteneur en indiquant une autre commande à la suite de la commande `docker run` (par exemple `echo "Salut !"`). Corriger le problème pour que le `echo` puisse fonctionner.


## 2) Conteneuriser une application

Soit l'application qu'on retrouve dans ce dépôt Git : `https://gitlab.com/formationdok/Express_Mongo`

Il s'agit d'une application Node.js qui fonctionne avec une base de données MongoDB.

On souhaite conteneuriser cette application.

* Cloner le repository Git de l'application en local sur sa machine avec `git clone https://gitlab.com/formationdok/Express_Mongo`
* Aller dans le répertoire de l'application qui vient d'être téléchargé et écrire le Dockerfile afin de conteneuriser cette application. Quelques indications :
  * On utilisera de base une image `node:16`
  * Au niveau de l'image, on travaille dans le répertoire `/usr/src/app`
  * On définit la connexion à la base de données avec la variable d'environnement `MONGO_URI` (par défaut `mongodb://mongo:27017` si non renseigné)
  * On spécifie le port d'écoute de l'application avec la variable d'environnement `PORT` (par défaut `6060`)
  * La commande qui permet de télécharger les dépendances du projet : `npm ci`
  * La commande qui permet de lancer l'application : `npm start`
* Essayer de lancer un conteneur de notre application en publiant le port 6060 avec un `docker run` et observer le résultat
* On va monter une petite infrastructure pour tester notre application en lançant un serveur MongoDB
  * Créer un réseau bridge `app`
  * Lancer un conteneur `mongo` basé sur une image `mongo:latest` sur ce réseau bridge `app`
  * Lancer un conteneur de notre application sur ce réseau bridge `app` avec en publiant le port 6060 vers le port 6060 du serveur Docker
  * Dans le navigateur, essayer d'aller sur `http://localhost:6060` et observer le résultat
* Supprimer le conteneur `mongo` et le conteneur de l'application
* Relancer un conteneur basé sur l'image `mongo` avec cette fois-ci `mongoapp` comme nom, toujours sur le réseau `app`
* Relancer le conteneur de l'application, en mettant les bonnes variables d'environnement, pour qu'il écoute sur le port `6789`

## 3) Implémenter un HEALTHCKECK

On va essayer d'implémenter un _health check_ pour notre application

Dans le répertoire de l'application, on trouve un fichier `healthcheck.js`. Lancer ce fichier permet de faire une simple requête HTTP sur _localhost_ pour retourner `0` si le code retour est 200, `1` sinon.

* Modifier le _Dockerfile_ de l'application pour lancer ce _healthcheck_ toutes les 15 secondes, après que le conteneur ait été lancé depuis 30 secondes. On règlera un _timeout_ de 2 secondes. On lancera la commande `node healthcheck.js`
* Reconstruire l'image
* Relancer l'application avec le serveur MongoDB associé
* Vérifier que le conteneur est en bonne santé