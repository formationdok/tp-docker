# TP6 - Docker Swarm

## Mise en oeuvre

Pour exécuter ce TP, nous aurons besoin de plusieurs machines. Pour cela, on utilisera [_Play With Docker_](https://github.com/play-with-docker/play-with-docker).

Deux possibilités :
- Utiliser [_Play With Docker_ fourni par Docker](https://labs.play-with-docker.com/)
- Utiliser _Play With Docker_ fourni par le formateur (solution privilégiée lors des formations)

## Mise en palce d'un swarm

* Initialiser un swarm sur la première machine
* Vérifier la liste des noeuds présents
* Vérifier via un `docker info` que la machine fait vien partie d'un swarm
* Créer 3 machines supplémentaires, récupérer le jeton pour en faire des _workers_ et les initialiser en tant que _worker_
* Vérifier la liste des noeuds présents
* Sur le swarm, créer un service _web_, basé sur l'image `nginx` avec 2 replicas. Le service devra exposer le port 8080 vers le port 80 des _workers_
* Sur le _manager_, vérifier que le service est bien opérationnel et afficher ses informations
* Essayer de se connecter à l'application via le port 8080 du _manager_

## Simulation de panne

* Sur le _manager_, vérifier les noeuds sur lesquels est opérationnel `nginx`
* Couper un _worker_ sur lequel `nginx` est opérationnel
* Vérifier que le swarm a bien remis le nombre de replicas à 2

## Redimensionnement du swarm et arrêt

* Modifier le swarm pour qu'il y ait maintenant 3 replicas
* Vérifier la présence de 3 replicas
* Arrêter le service `nginx` et vérifier que les conteneurs ont bien été arrêtés sur les _workers_.

## Création d'un docker-compose et mise à jour

* Effacer toutes les machines créés précédemment sur _Play With Docker_
* Créer une machine sur _Play With Docker_ puis y créer un dossier `app` avec à l'intérieur :
  * Un fichier `docker-compose.yml` (vide pour le moment)
  * Un dossier myweb avec à l'intérieur
    * Un fichier `Dockerfile`
    * Un fichier `index.html`

  Voici le contenu du fichier `index.html`

  ```
  <h1>Version 1.0</h1>
  ```

  Voici le contenu du fichier `Dockerfile`

  ```
  FROM nginx:1.25.2
  COPY index.html /usr/share/nginx/html/
  ```

* Faire un build de l'image et la pousser sur le Docker Hub (éventuellement la tester avant le push)
* Ecrire le fichier `docker-compose.yml` afin qu'il utilise l'image qu'on a créé, avec un nombre de replicas à 2 (ne pas oublier d'exposer le port 8080 vers le port 80 de nos conteneurs)
* Créer 3 machines supplémentaires sur _Play With Docker_ et initialiser un swarm avec 1 _manager_ et 3 _workers_
* Via la commande `docker stack deploy`, déployer le service décrit dans le fichier `docker-compose.yml`
* Vérifier que le site est accessible
* Modifier le fichier `index.html` et changer la version en 2.0, recréer l'image et la pousser sur le repository Docker Hub
* Mettre à jour le service swarm avec la version 2.0 fraichement créée avec `docker service update` puis tester

