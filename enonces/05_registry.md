# TP5 - Registre privé

## Quelques astuces :

  - Liste des commandes pour mettre à jour et installer des packages :
    - debian : `apt update && apt install -y nom-du-package`
    - alpine : `apk update && apk add nom-du-package`
    - archlinux : `pacman -Syu --noconfirm  && pacman -S  --noconfirm nom-package` <br>

## 1) Création d'un registre privé

  * Aller sur dockerhub et rechercher l'image `registry`
  * Lancer le conteneur avec les paramètres suivants
    * mode détaché
    * exposition/redirection de port : **5000**
    * politique de redémarrage : **toujours**  _(commande `--restart=always`)_
    * version de l'image de base : **2.6.2**
    * sauvegarder cette commande `docker container run [..]` quelque par pour plus tard
  * lister les images présentes dans le nouveau registre
    * commande : `curl -X GET http://localhost:5000/v2/_catalog`

## 2) Déposer une image dans le Registry privé

  * niveau 0 :
    * Créer une image personnalisée : `alpine` &rarr; dans laquelle le paquet `git` sera installé
      * en ligne de commande ou via Dockerfile, au choix
      * nom de l'image en sortie : **alpinegit** <br> <br>
      * vérifier la version de git _(`git --version`)_

  * Créer un nouveau tag pour l'image, pour pouvoir l'envoyer ensuite sur le registry
    * syntaxe : `docker tag <IMAGE NAME> <SERVER NAME REGISTRY>:<PORT SERVER REGISTRY>/<IMAGE NAME>`
  * Vérifier que le tag est bien mis dans la liste des images
  * Pousser l'image dans le nouveau registry
    * Vérifier la liste des images dans le registry
  * Supprimer l'image **locale** alpinegit
  * Effectuer un pull pour la récupérer depuis le registry
  * La lancer et vérifier que git est bien présent en affichant sa version
  * Créer un nouveau tag **test** sur l'image alpinegit
    * la pousser de nouveau
    * vérifier la liste des images du registry, ainsi que la liste des tags via la commande `curl -X GET http://localhost:5000/v2/<IMAGE-NAME>/tags/list`
  * sauvegarder l'image en archive <br> <br>
  * Arrêter le conteneur du registry privé

 * niveau 1 :
   * Faire le niveau 0
   * Refaire les mêmes manipulations pour obtenir une image : `alpine` + `MySql`
     * Vérifier qu'il y a bien deux images dans le registry

 * niveau 2 :
   * ajouter un volume pour la persistence des données _(en effet, lors de l'arret du conteneur, tout sera perdu)_
     * mapping : `<le-dossier-choisi>:/var/lib/registry`
   * refaire le niveau 0 _(vérifier que tout a bien été perdu lors de l'arrêt du conteneur)_
   * redémarrer le conteneur du registre privé _(merci la commande sauvegardée plus tôt)_
     * vérifier qu'**alpinegit** est toujours présente
