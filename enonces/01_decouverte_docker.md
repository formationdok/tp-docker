# TP 1
## 1) Manipulation des commandes

 * Afficher la version de docker
 * Afficher l'aide

## 2) Lancement du 1er conteneur

 * Lister les images déjà téléchargées. Vérifier qu'il n'en existe aucune, sinon les supprimer toutes
 * Lancer le conteneur `hello-world`. Regarder et interpréter ce qu'il se passe
 * Lister les images déjà téléchargées. Vérifier qu'il en existe une
 * Afficher les informations détaillées de l'image
 * Relancer le conteneur `hello-world`. Regarder et interpréter ce qu'il se passe

## 3) Lancement plus avancé

 * Chercher sur docker hub une image contenant `apache`
 * Récupérer l'image choisie depuis Docker Hub
 * Le lancer en mode détaché
 * Vérifier la liste des conteneurs lancés
 * Vérifier qu'en allant dans un navigateur _([http://localhost](http://localhost))_ on obtient une erreur
 * Arreter le conteneur apache, puis le supprimer
 * Le recréer en lui donnant comme nom `server-web`, en exposant le port 80 avec le paramètre `-p 80:80` _(on verra ça lors du chapitre sur le réseau)_
 * Vérifier que celui-ci est bien lancé en allant dans un navigateur : [http://localhost](http://localhost)
 * Supprimer le conteneur **server-web**

## 4) Mode interactif

  * Exécuter docker pour obtenir un conteneur avec **archlinux**, en mode attaché et interactif.
  * Lister le répertoire courant
  * Obtenir la version de la distribution. Au choix :
    * `cat /etc/os-release`
    * `cat /etc/issue`
  * Vérifier les valeurs des variables d'environnement suivantes : `${HOME}`, `${HOSTNAME}`, `${JAVA_HOME}` _(via `echo`)_
  * Vérifier que la commande mise à jour de centos ne fonctionne pas : `yum update`
    * idem pour celle de debian : `apt update`
  * Vérifier que celle pour archlinux fonctionne : `pacman -Syu`  
  * Sortir du conteneur

## 5) Variables d'environnement
 * Recréer le conteneur archlinux en changeant les valeurs des variables d'environnement :
   * **HOME** &rarr; `/var`
   * **HOSTNAME** &rarr; `hello-arch`
   * **JAVA_HOME** &rarr; `/opt/openjdk11`

## 6) Gestion des données
  * Créer un ou deux fichiers puis sortir du conteneur
  * Revenir dessus en interprétant le résultat des deux façons de faire :
    * en relançant la commande `docker run` &rarr; vérifier que les fichiers ont disparu
    * en lançant les commandes 
      ```sh
      docker ps -a 
      docker start <nom-du-conteneur>
      docker attach <nom-du-conteneur>
      ```
       &rarr; vérifier que les fichiers sont toujours présents

## 7) Volumes
 * Créer un volume et l'associer au conteneur
   * Refaire les mêmes manipulations dans ce volume _(tout en changeant les noms de fichiers ainsi que leur contenu)_ et vérifier que les fichiers sont toujours présents, dans le volume créé.
   * Créer un conteneur **debian**, associer ce volume en lecture seule, vérifier que les fichiers sont bien présents
   * Ajouter de nouveaux fichiers et sortir du conteneur debian
   * Retourner sur le conteneur archlinux. Les nouveaux fichiers provenant de debian n'existent pas
   * Supprimer le volume

## 8) Bind mounts
 * Associer un répertoire entre l'ordinateur hôte et le conteneur archlinux
   * Refaites les mêmes manipulations que précédemment _(sans la partie "lecture seule")_
   * Créer un conteneur **alpine**, associer ce répertoire et vérifier que les fichiers sont bien présents
     * utiliser la syntaxe `--mount` au lieu de `-v`
