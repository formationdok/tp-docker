# TP4 - Application multiconteneur

## 1) Mise en place d'une architecture

  * On souhaite créer une architecture comprenant un serveur _Wordpress_, adossée à une base de données _MariaDB_.
    * On souhaite utiliser les images suivantes :
      - `wordpress:6.2`
      - `mariadb:11.0`
  * Ecrire le fichier `docker-compose.yml` afin de mettre en place cette architecture. On évitera au maximum de regarder le `docker_compose.yml` de la documentation de l'image `Wordpress`...
    * Quelques indications pour _Wordpress_ :
      - On mettra la politique de redémarrage à `always`.
      - L'image `Wordpress` écoute sur le port `80`. On fera un mapping de vers le port `8085` du serveur Docker.
      - La connexion à la base de données est renseignée avec quelques variables d'environnement :
        - `WORDPRESS_DB_HOST`
        - `WORDPRESS_DB_USER`
        - `WORDPRESS_DB_PASSWORD`
        - `WORDPRESS_DB_NAME`
      - On utilisera un volume pour stocker les fichiers du site web. On utilisera un volume pour le répertoire `/var/www/html` du serveur.
    * Quelques indications pour _MariaDB_ :
      - On mettra la politique de redémarrage à `always`.
      - Utiliser les bonnes variables d'environnement pour créer l'utilisateur utilisé par _Wordpress_, ainsi qu'un mot de passe `root` aléatoire.
      - On utilisera un volume pour stocker les fichiers du site web. On utilisera un volume pour le répertoire `/var/lib/mysql` du serveur.
  * Tester le lancement de ce fichier et le bon fonctionnement de _Wordpress_.
  * Faire ce qu'il faut pour récupérer le mot de passe `root` _MariaDB_ généré.
  * Arrêter tous les services sans supprimer les volumes

## 2) Amélioration de l'architecture

  * On souhaite améliorer l'architecture avec les modifications suivantes (tester le fichier après chaque amélioration ):
    * Au lieu d'utiliser un réseau créé pour l'occasion automatiquement, on souhaite utiliser un réseau `bridge` appelé `wpnet`. 
    * Changer les _hostnames_ des serveurs _Wordpress_ et _MariaDB_ comme suit et modifier le fichier `docker-compose.yml` en conséquence.
      - _Wordpress_ : `pistache`
      - _MariaDB_: `noisette`
    * Modifier le nom des conteneurs créés :
      - _Wordpress_ : `formationwp`
      - _MariaDB_: `formationdb`
    * Coupler un serveur _PHPMyAdmin_ à la base de données _MariaDB_ via l'utilisation de l'image `phpmyadmin:5.2` et ces précisions :
      - _Hostname_ du serveur : `cajou`
      - Politique de redémarrage à `always`.
      - Nom du conteneur : `formationphpma`
      - L'image `phpmyadmin` écoute sur le port `80`. On fera un mapping de vers le port `8086` du serveur Docker.
      - Trouver ce qu'il faut pour relier _PHPMyAdmin_ à _MariaDB_.
    * Tester la connexion à _PHPMyAdmin_

## 3) Utilisation d'images personnelles

Dans le TP3, exercices 2 et 3, on a créé un `Dockerfile` d'un serveur _Node.js_.

Créer le fichier `compose.yaml` permettant de faire fonctionner tout cela en y ajoutant un conteneur généré par ce `Dockerfile`. Quelques précisions :
  - _Hostname_ du serveur Node.js : `macadamia`
  - _Hostname_ du serveur MongoDB : `pistache`
  - Politique de redémarrage à `always`
  - Nom du conteneur Node.js : `formationnode`
  - Nom du conteneur MongoDB : `formationmongodb`
  - Node.js écoute sur le port `9500`. On fera un mapping de vers le port `8088` du serveur Docker
  - On utilisera un volumme appelé `mongovol` pour persister les données MongoDB
  - On utilisera un réseau appelé `formnet`

Mettre en place l'architecture via un fichier `compose.yaml` et vérifier le bon fonctionnement de tous les conteneurs.
